<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('WP_ALLOW_REPAIR', true);

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'himalayamagdb' );

/** MySQL database username */
define( 'DB_USER', 'wpdbadmin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wpdbadmin' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~6-_wv8?O1xReIA1H8)c%L_:i_FO-jHCACu?,y  X%X>ryD2Bhb<Fs0[>NqnMwY@' );
define( 'SECURE_AUTH_KEY',  'Z=OLxOV;0/t2dmnmKYx$lU95|lyhPPic|`uh}f0um0b+T~iypCjjSd{`b{KQ>j&g' );
define( 'LOGGED_IN_KEY',    'I)Nl 0z@E==9Pt 4e_c9DIRc)WPk!44CH>P,aLFLfB]4d)XE#+9ORf/d1{/+G3q|' );
define( 'NONCE_KEY',        'uYGmY%h&Q{$ij>cf]gA)uyG=;QM8TQ}lvv-=VaXML,lx=$6_10h?%LXAe0c8HGa}' );
define( 'AUTH_SALT',        'gP61>NwQxg6^2KsXXt<uh2qr8d7 ./b8h_;R/00Xczq[*Hy#d?KySoI$rOQ:+fJZ' );
define( 'SECURE_AUTH_SALT', 'hk~h;9qj,A 9A%~SQ]R;%&4)>ucn6xRNR{p[I6>[Au[Ow<9@,JaWdeA>H*.2S/4v' );
define( 'LOGGED_IN_SALT',   'p&*yDthA/i>Ygo55W1wrxgc}Un=icJSyyhK-J32@BP: I,[)B[?0TeHk<)y>=Vyb' );
define( 'NONCE_SALT',       'c15CkL][nk%o<EmUu*;wPC-hff+-2@0b|PF@pf7Z3oR*75(#E$;(YP2l2FiP(&kj' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'hmg_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
