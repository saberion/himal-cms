<?php
/**
 * Template Name: Careers  Page
 *
 * This is the template that displays for careers  page
 *
 * @package sparkling
 */

get_header(); ?>



<div class="fullwidth careers-page inner_page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="fullwidth section-heading">
                    <h2><span>Careers at HIMĀL </span></h2>
                </div>

            </div>



            <?php
            $args = array( 'post_type' => 'vacancies', 'posts_per_page'   =>  -1, );
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

            <div class="col-md-12 careerblock">

                <div class="career-title fullwidth">
                    <h2><?php the_title(); ?></h2>
                </div>

                <div class="fullwidth career-description">
                    <p><?php the_field('introduction');?></p>
                </div>

                <div class="fullwidth resposibility_list">
                    <?php the_field('major_responsibilities');?>
                </div>

                <div class="fullwidth requiremnts_list">

                    <?php
                    if( have_rows('requirements') ): while ( have_rows('requirements') ) : the_row();
                    ?>

                    <div class="requremntblock fullwidth">
                        <div class="require_heading"><h3><?php the_sub_field('heading');?></h3></div>
                        <div class="require_info"><?php the_sub_field('requirement_point');?></div>
                    </div>

                        <?php
                    endwhile; endif;?>

                </div>

                <div class="fullwidth other_info">
                    <?php the_field('other_details');?>
                </div>

                <div class="sharebox fullwidth">
                    <span> Share this job :</span>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>careers" class="share-fb" title="<?php the_title();  ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/share-icon-fb.png"></a>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>careers" class="share-tw" title="<?php the_title();  ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/share-icon-tw.png"></a>
                </div>

            </div>


            <?php endforeach;
            wp_reset_postdata();
            ?>




        </div>
    </div>
</div>


<?php include('membership.php'); ?>

<?php
get_footer();

?>
