<?php
/**
 * Template Name: Seylan IPG Request Page
 *
 * This is the template that Seylan IPG Request page
 *
 */

get_header();

$membership_id = $_REQUEST['membership_id'];

if (!$membership_id) {
	$membership_id = 2;
}

?>


    <div class="archive-wrap fullwidth ipg-pages">

        <div class="fullwidth payment-title-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 title-txt">
                        <p>One step closer to your <i>Himal</i> membership</p>
                    </div>
                    <div class="col-md-12 title-img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/payment-title-img.png"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="fullwidth payment-option-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Thank you for choosing the following membership package</h2>
                    </div>
                </div>
            </div>
        </div>


    <div class="fullwidth payment-option-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12 payment-option-list">
                    <a id="memeberblock-id" data-id="membership-<?php echo $membership_id; ?>"></a>


            <?php
            $args = array( 'post_type' => 'membership_page', 'posts_per_page'   =>  1);
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post );
                ?>

                <?php
                if( have_rows('membership_plans') ):
                    while  ( have_rows('membership_plans') ) : the_row()  ;
                        ?>

                    <div class="option-block-wrap fullwidth" id="membership-<?php the_sub_field('link'); ?>">
                        <div class="fullwidth">
                            <a class="reqst-head">
                                <strong>  <?php the_sub_field('name'); ?></strong><br/>
                                <span>$  <?php the_sub_field('price'); ?></span>
                            </a>
                        </div>
                        <div class="fullwidth request-more-info" >
                            <?php the_sub_field('more_details'); ?>
                        </div>
                    </div>

                        <?php
                    endwhile;
                else :
                endif;
                ?>

            <?php endforeach;
            wp_reset_postdata();
            ?>

                </div>
            </div>
        </div>
    </div>



        <div class="fullwidth  article-info-box ipg-info-box " >
            <div class="container">
                <div class="row">
                    <div class="col-md-12 article-content-wrap" id="main-article-content">


                        <!-- -->
                        <div class="fullwidth">
                            <div class="container-inner" id="error_messages"></div>
                        </div>
                        <!-- -->


                        <form name="frmIPGRequest" action="#" method="post">

                            <div class="fullwidth heading-box">
                                <div class="container-inner">
                                    <div class="fullwidth title-row" >
                                        <h3>Your Information <span>Will be used for your login</span></h3>
                                    </div>
                                </div>
                            </div>


                        <div class="fullwidth  default-page-data  upme-wrap">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="fullwidth ipg-reqest">
                                        <div class="fullwidth ipg-reqest">
                                            <div class="row"><div class="col-md-12">
                                                    <input type="email" id="email" name="email" value="" placeholder="Email">
                                                    <div class="error-msg"></div>
                                                </div> </div>
                                            <div class="row"><div class="col-md-6">
                                                    <input type="text" id="firstName" name="firstName" value="" placeholder="First Name"><div class="error-msg"></div></div>
                                                <div class="col-md-6">
                                                    <input type="text" id="lastName" name="lastName" value="" placeholder="Last Name">
                                                    <div class="error-msg"></div>
                                                </div></div>
                                            <div class="row"><div class="col-md-6"><input type="hidden" id="membership_id" name="membership_id" value="<?php echo $membership_id; ?>">
                                                    <input type="password" id="password" name="password" value="" placeholder="Password">
                                                    <div class="error-msg"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="password" id="rePassword" name="rePassword" value="" placeholder="Confirm Password">
                                                    <div class="error-msg"></div>
                                                </div></div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                            <div class="col-md-12"> <span>Will be used in accordance with our
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=26311" target="_blank">privacy policy </a></span>
                            </div>
                            </div>
                        </div>


                            <div class="fullwidth heading-box">
                                <div class="container-inner">
                                    <div class="fullwidth title-row" >
                                        <h3>Delivery Address <span>Where we will ship your Himal Right Side Up map.</span></h3>
                                    </div>
                                </div>
                            </div>

                            <div class="fullwidth  default-page-data  upme-wrap">
                                <div class="row">
                                <div class="col-md-12 ipg-form-side">
                                    <div class="fullwidth ipg-reqest">
                                        <div class="row"><div class="col-md-6"><input type="text" id="phone" name="phone" value="" placeholder="Phone">
                                                <div class="error-msg"></div>
                                                </div>
                                            <div class="col-md-6">
                                                <input type="text" id="address1" name="address1" value="" placeholder="Address 1">
                                                <div class="error-msg"></div>
                                               </div></div>
                                        <div class="row"><div class="col-md-6">   <input type="text" id="address2" name="address2" value="" placeholder="Address 2">
                                                <div class="error-msg"></div>
                                            </div>
                                            <div class="col-md-6"><input type="text" id="city" name="city" value="" placeholder="City">
                                                <div class="error-msg"></div>
                                            </div></div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" id="county" name="county" value="" placeholder="County/State/Province">
                                                <div class="error-msg"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="country" id="country">
                                                    <option value="">Select Country</option>
                                                    <option value="Afghanistan">Afghanistan</option>
                                                    <option value="Albania">Albania</option>
                                                    <option value="Algeria">Algeria</option>
                                                    <option value="Andorra">Andorra</option>
                                                    <option value="Angola">Angola</option>
                                                    <option value="Antigua & Deps">Antigua & Deps</option>
                                                    <option value="Argentina">Argentina</option>
                                                    <option value="Armenia">Armenia</option>
                                                    <option value="Australia">Australia</option>
                                                    <option value="Austria">Austria</option>
                                                    <option value="Azerbaijan">Azerbaijan</option>
                                                    <option value="Bahamas">Bahamas</option>
                                                    <option value="Bahrain">Bahrain</option>
                                                    <option value="Bangladesh">Bangladesh</option>
                                                    <option value="Barbados">Barbados</option>
                                                    <option value="Belarus">Belarus</option>
                                                    <option value="Belgium">Belgium</option>
                                                    <option value="Belize">Belize</option>
                                                    <option value="Benin">Benin</option>
                                                    <option value="Bhutan">Bhutan</option>
                                                    <option value="Bolivia">Bolivia</option>
                                                    <option value="Bosnia Herzegovina">Bosnia Herzegovina</option>
                                                    <option value="Botswana">Botswana</option>
                                                    <option value="Brazil">Brazil</option>
                                                    <option value="Brunei">Brunei</option>
                                                    <option value="Bulgaria">Bulgaria</option>
                                                    <option value="Burkina">Burkina</option>
                                                    <option value="Burundi">Burundi</option>
                                                    <option value="Cambodia">Cambodia</option>
                                                    <option value="Cameroon">Cameroon</option>
                                                    <option value="Canada">Canada</option>
                                                    <option value="Cape Verde">Cape Verde</option>
                                                    <option value="Central African Rep">Central African Rep</option>
                                                    <option value="Chad">Chad</option>
                                                    <option value="Chile">Chile</option>
                                                    <option value="China">China</option>
                                                    <option value="Colombia">Colombia</option>
                                                    <option value="Comoros">Comoros</option>
                                                    <option value="Congo">Congo</option>
                                                    <option value="Congo {Democratic Rep}">Congo {Democratic Rep}</option>
                                                    <option value="Costa Rica">Costa Rica</option>
                                                    <option value="Croatia">Croatia</option>
                                                    <option value="Cuba">Cuba</option>
                                                    <option value="Cyprus">Cyprus</option>
                                                    <option value="Czech Republic">Czech Republic</option>
                                                    <option value="Denmark">Denmark</option>
                                                    <option value="Djibouti">Djibouti</option>
                                                    <option value="Dominica">Dominica</option>
                                                    <option value="Dominican Republic">Dominican Republic</option>
                                                    <option value="East Timor">East Timor</option>
                                                    <option value="Ecuador">Ecuador</option>
                                                    <option value="Egypt">Egypt</option>
                                                    <option value="El Salvador">El Salvador</option>
                                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                    <option value="Eritrea">Eritrea</option>
                                                    <option value="Estonia">Estonia</option>
                                                    <option value="Ethiopia">Ethiopia</option>
                                                    <option value="Fiji">Fiji</option>
                                                    <option value="Finland">Finland</option>
                                                    <option value="France">France</option>
                                                    <option value="Gabon">Gabon</option>
                                                    <option value="Gambia">Gambia</option>
                                                    <option value="Georgia">Georgia</option>
                                                    <option value="Germany">Germany</option>
                                                    <option value="Ghana">Ghana</option>
                                                    <option value="Greece">Greece</option>
                                                    <option value="Grenada">Grenada</option>
                                                    <option value="Guatemala">Guatemala</option>
                                                    <option value="Guinea">Guinea</option>
                                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                    <option value="Guyana">Guyana</option>
                                                    <option value="Haiti">Haiti</option>
                                                    <option value="Honduras">Honduras</option>
                                                    <option value="Hungary">Hungary</option>
                                                    <option value="Iceland">Iceland</option>
                                                    <option value="India">India</option>
                                                    <option value="Indonesia">Indonesia</option>
                                                    <option value="Iran">Iran</option>
                                                    <option value="Iraq">Iraq</option>
                                                    <option value="Ireland {Republic}">Ireland {Republic}</option>
                                                    <option value="Israel">Israel</option>
                                                    <option value="Italy">Italy</option>
                                                    <option value="Ivory Coast">Ivory Coast</option>
                                                    <option value="Jamaica">Jamaica</option>
                                                    <option value="Japan">Japan</option>
                                                    <option value="Jordan">Jordan</option>
                                                    <option value="Kazakhstan">Kazakhstan</option>
                                                    <option value="Kenya">Kenya</option>
                                                    <option value="Kiribati">Kiribati</option>
                                                    <option value="Korea North">Korea North</option>
                                                    <option value="Korea South">Korea South</option>
                                                    <option value="Kosovo">Kosovo</option>
                                                    <option value="Kuwait">Kuwait</option>
                                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                    <option value="Laos">Laos</option>
                                                    <option value="Latvia">Latvia</option>
                                                    <option value="Lebanon">Lebanon</option>
                                                    <option value="Lesotho">Lesotho</option>
                                                    <option value="Liberia">Liberia</option>
                                                    <option value="Libya">Libya</option>
                                                    <option value="Liechtenstein">Liechtenstein</option>
                                                    <option value="Lithuania">Lithuania</option>
                                                    <option value="Luxembourg">Luxembourg</option>
                                                    <option value="Macedonia">Macedonia</option>
                                                    <option value="Madagascar">Madagascar</option>
                                                    <option value="Malawi">Malawi</option>
                                                    <option value="Malaysia">Malaysia</option>
                                                    <option value="Maldives">Maldives</option>
                                                    <option value="Mali">Mali</option>
                                                    <option value="Malta">Malta</option>
                                                    <option value="Marshall Islands">Marshall Islands</option>
                                                    <option value="Mauritania">Mauritania</option>
                                                    <option value="Mauritius">Mauritius</option>
                                                    <option value="Mexico">Mexico</option>
                                                    <option value="Micronesia">Micronesia</option>
                                                    <option value="Moldova">Moldova</option>
                                                    <option value="Monaco">Monaco</option>
                                                    <option value="Mongolia">Mongolia</option>
                                                    <option value="Montenegro">Montenegro</option>
                                                    <option value="Morocco">Morocco</option>
                                                    <option value="Mozambique">Mozambique</option>
                                                    <option value="Myanmar, {Burma}">Myanmar, {Burma}</option>
                                                    <option value="Namibia">Namibia</option>
                                                    <option value="Nauru">Nauru</option>
                                                    <option value="Nepal">Nepal</option>
                                                    <option value="Netherlands">Netherlands</option>
                                                    <option value="New Zealand">New Zealand</option>
                                                    <option value="Nicaragua">Nicaragua</option>
                                                    <option value="Niger">Niger</option>
                                                    <option value="Nigeria">Nigeria</option>
                                                    <option value="Norway">Norway</option>
                                                    <option value="Oman">Oman</option>
                                                    <option value="Pakistan">Pakistan</option>
                                                    <option value="Palau">Palau</option>
                                                    <option value="Panama">Panama</option>
                                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                                    <option value="Paraguay">Paraguay</option>
                                                    <option value="Peru">Peru</option>
                                                    <option value="Philippines">Philippines</option>
                                                    <option value="Poland">Poland</option>
                                                    <option value="Portugal">Portugal</option>
                                                    <option value="Qatar">Qatar</option>
                                                    <option value="Romania">Romania</option>
                                                    <option value="Russian Federation">Russian Federation</option>
                                                    <option value="Rwanda">Rwanda</option>
                                                    <option value="St Kitts & Nevis">St Kitts & Nevis</option>
                                                    <option value="St Lucia">St Lucia</option>
                                                    <option value="Saint Vincent & the Grenadines">Saint Vincent & the Grenadines</option>
                                                    <option value="Samoa">Samoa</option>
                                                    <option value="San Marino">San Marino</option>
                                                    <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                                    <option value="Senegal">Senegal</option>
                                                    <option value="Serbia">Serbia</option>
                                                    <option value="Seychelles">Seychelles</option>
                                                    <option value="Sierra Leone">Sierra Leone</option>
                                                    <option value="Singapore">Singapore</option>
                                                    <option value="Slovakia">Slovakia</option>
                                                    <option value="Slovenia">Slovenia</option>
                                                    <option value="Solomon Islands">Solomon Islands</option>
                                                    <option value="Somalia">Somalia</option>
                                                    <option value="South Africa">South Africa</option>
                                                    <option value="South Sudan">South Sudan</option>
                                                    <option value="Spain">Spain</option>
                                                    <option value="Sri Lanka">Sri Lanka</option>
                                                    <option value="Sudan">Sudan</option>
                                                    <option value="Suriname">Suriname</option>
                                                    <option value="Swaziland">Swaziland</option>
                                                    <option value="Sweden">Sweden</option>
                                                    <option value="Switzerland">Switzerland</option>
                                                    <option value="Syria">Syria</option>
                                                    <option value="Taiwan">Taiwan</option>
                                                    <option value="Tajikistan">Tajikistan</option>
                                                    <option value="Tanzania">Tanzania</option>
                                                    <option value="Thailand">Thailand</option>
                                                    <option value="Togo">Togo</option>
                                                    <option value="Tonga">Tonga</option>
                                                    <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                                    <option value="Tunisia">Tunisia</option>
                                                    <option value="Turkey">Turkey</option>
                                                    <option value="Turkmenistan">Turkmenistan</option>
                                                    <option value="Tuvalu">Tuvalu</option>
                                                    <option value="Uganda">Uganda</option>
                                                    <option value="Ukraine">Ukraine</option>
                                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                                    <option value="United Kingdom">United Kingdom</option>
                                                    <option value="United States">United States</option>
                                                    <option value="Uruguay">Uruguay</option>
                                                    <option value="Uzbekistan">Uzbekistan</option>
                                                    <option value="Vanuatu">Vanuatu</option>
                                                    <option value="Vatican City">Vatican City</option>
                                                    <option value="Venezuela">Venezuela</option>
                                                    <option value="Vietnam">Vietnam</option>
                                                    <option value="Yemen">Yemen</option>
                                                    <option value="Zambia">Zambia</option>
                                                    <option value="Zimbabwe">Zimbabwe</option>
                                                </select>
                                                <div class="error-msg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>

                                <div class="row">
                                <div class="col-md-12">
                                    <div class="fullwidth ipg-reqest">
                                        <input type="submit" name="btnPay" class="upme-button" id="btnPay" value="Become a Member">
                                    </div>
                                </div>
                                </div>

                                <div class="row">
                                <div class="col-md-12"> <span>By signing up to our membership you are agreeing to Himal Southasian's <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=26311" target="_blank">membership policies</a>
                                        and <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=26302" target="_blank">terms & conditions</a>. </span>
                                </div>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
