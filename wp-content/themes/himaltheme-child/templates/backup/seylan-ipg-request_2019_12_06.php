<?php
/**
 * Template Name: Seylan IPG Request Page
 *
 * This is the template that Seylan IPG Request page
 *
 */

get_header();

$membership_id = $_REQUEST['membership_id'];

if (!$membership_id) {
	$membership_id = 2;
}

?>

<div>
	<form name="frmIPGRequest" action="#" method="post">
		<input type="text" id="userName" name="userName" value="" placeholder="username">
		<input type="email" id="email" name="email" value="" placeholder="Email">
		<input type="password" id="password" name="password" value="" placeholder="Password">
		<input type="hidden" id="membership_id" name="membership_id" value="<?php echo $membership_id; ?>">
		<input type="text" id="firstName" name="firstName" value="" placeholder="First Name">
		<input type="text" id="lastName" name="lastName" value="" placeholder="Last Name">
		<input type="text" id="address1" name="address1" value="" placeholder="Address 1">
		<input type="text" id="address2" name="address2" value="" placeholder="Address 2">
		<input type="text" id="city" name="city" value="" placeholder="city">
		<input type="text" id="county" name="county" value="" placeholder="County">
		<input type="text" id="postalCode" name="postalCode" value="" placeholder="Postal Code">
		<input type="text" id="country" name="country" value="" placeholder="Country">
		<input type="text" id="phone" name="phone" value="" placeholder="Phone">
		<input type="submit" name="btnPay" id="btnPay" value="Pay">
	</form>
</div>

<?php
get_footer();