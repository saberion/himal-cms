<?php
/**
 * Template Name: Seylan IPG Confirm Page
 *
 * This is the template that Seylan IPG Confirm page
 *
 */

get_header();

$user_id = $_REQUEST['user_id'];
$memberships_users_id = $_REQUEST['memberships_users_id'];
$membership_orders_id = $_REQUEST['membership_orders_id'];

$pgdomain = "fedpg-onus.pc.enstage-sas.com";
$pgInstanceId = 37166521;
$merchantId = 94024444;
$hashKey = "5F5C3CA127EE21AE";

$perform = "initiatePaymentCapture#sale";
$currencyCode = 356;
$amount = 30000;
$merchantReferenceNo = "AX143565";
$orderDesc = "Membership Payment";

$messageHash = $pgInstanceId."|".$merchantId."|".$perform."|".$currencyCode."|".$amount."|".$merchantReferenceNo."|".$hashKey;

$message_hash = "STANDARD:6:".base64_encode(SHA1($messageHash, true));

?>

<form name="merchantForm" method="post" action="https://<?php echo $pgdomain;?>/AccosaPG/verify.jsp">

	<input type="hidden" name="pg_instance_id" value="<?php echo $pgInstanceId;?>" />
	<input type="hidden" name="merchant_id" value="<?php echo $merchantId;?>" />
	<input type="hidden" name="perform" value="<?php echo $perform;?>" />
	<input type="hidden" name="currency_code" value="<?php echo $currencyCode;?>" />
	<input type="hidden" name="amount" value="<?php echo $amount;?>" />
	<input type="hidden" name="merchant_reference_no" value="<?php echo $merchantReferenceNo;?>" />
	<input type="hidden" name="order_desc" value="<?php echo $orderDesc;?>" />
	<input type="hidden" name="message_hash" value="<?php echo $message_hash;?>" />
	<input type="hidden" name="ext1" value="<?php echo $user_id;?>" />
	<input type="hidden" name="ext2" value="<?php echo $memberships_users_id; ?>" />
	<input type="hidden" name="ext3" value="<?php echo $membership_orders_id; ?>" />
	<input type="submit" name="btnConfirm" value="Confirm Payment" />
	</form>
<?php
get_footer();