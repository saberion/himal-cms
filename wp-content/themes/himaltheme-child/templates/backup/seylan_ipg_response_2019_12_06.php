<?php
/**
 * Template Name: Seylan IPG Response Page
 *
 * This is the template that Seylan IPG Response page
 *
 */

global $wpdb;

$user_id = $_POST['user_id'];
$memberships_users_id = $_POST['memberships_users_id'];
$membership_orders_id = $_POST['membership_orders_id'];

if ($user_id) {
	$user = new WP_User($user_id);
	$user->set_role('subscriber');
}

$wpdb->update('hmg_pmpro_memberships_users', array('id'=>$memberships_users_id, 'status'=> 'active'), array('id'=>$memberships_users_id));

$wpdb->update('hmg_pmpro_membership_orders', array('id'=>$membership_orders_id, 'status'=> 'success'), array('id'=>$membership_orders_id));

echo "<pre>";
print_r($_POST);
exit;