<?php
/**
 * Template Name: Journalists  Page
 *
 * This is the template that displays for journalists  page
 *
 * @package sparkling
 */

get_header(); ?>



    <div class="jounalist-page fullwidth">


        <div class="mob-desk-banner fullwidth">
            <div class="fullwidth banner-item banner-desktop"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/jounelist-banner-web.jpg"></div>
            <div class="fullwidth banner-item banner-mobile"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/jounelist-banner-mob.jpg"></div>

        </div>



        <!--<div class="fullwidth page-banner"  style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), "thumbnail" ); ?>')">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 banner-info">
                        <h1>Journalists <span>Under</span> Trial</h1>
                    </div>
                </div>
            </div>
        </div>-->


        <div class="fullwidth page-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-8 content-box">

                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php the_content(); ?>
                            <?php
                        endwhile;
                        wp_reset_query();
                        ?>
                    </div>


                    <div class="col-md-4">
                        <?php include('sidebar-newsletter-widget.php') ?>
                    </div>

                </div>
            </div>
        </div>




        <div class="fullwidth journelist-tables">
            <div class="container">

                <div class="row">

                    <div class="col-md-12 journal-table-block">


                            <?php
                            $custom_terms = get_terms('location');

                            foreach($custom_terms as $custom_term) {
                                wp_reset_query();
                                $args = array('post_type' => 'journalists_trial',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'location',
                                            'field' => 'slug',
                                            'terms' => $custom_term->slug,
                                        ),
                                    ),
                                );

                                $loop = new WP_Query($args);
                                if($loop->have_posts()) {
                                    ?>


                                    <div class="jounal-item fullwidth">

                                        <div class="journal-heading " >
                                            <h2><?php echo $custom_term->name ?></h2>
                                        </div>

                                        <div class="journal-info fullwidth" >

                                            <div class="jounal-info-txt">
                                                <p><?php echo $custom_term->description ?></p>
                                            </div>

                                            <table  class="jounaldatatable jouranl-info-table">
                                                <thead>

                                                <tr>
                                                    <th>Name</th>
                                                    <th>Organisation</th>
                                                    <th>Date</th>
                                                    <th>Party</th>
                                                    <th>Charge</th>
                                                    <th>Law/Case/Action</th>
                                                    <th>Status</th>
                                                </tr>

                                                </thead>


                                                <tbody>

                                                <?php
                                                while($loop->have_posts()) : $loop->the_post();
                                                ?>

                                            <tr>
                                                <td><?php the_title(); ?></td>
                                                <td><?php the_field('organisation'); ?></td>
                                                <td><?php the_field('date'); ?></td>
                                                <td><?php the_field('party'); ?></td>
                                                <td><?php the_field('charge'); ?></td>
                                                <td><?php the_field('law_case'); ?></td>
                                                <td><?php the_field('status'); ?></td>
                                            </tr>

                                                <?php
                                                endwhile;
                                                ?>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>

                                        <?php
                                }
                            }
                            ?>

                    </div>

                </div>
            </div>
        </div>




   </div>



<?php
get_footer();

?>
