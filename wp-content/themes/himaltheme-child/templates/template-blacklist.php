<?php
/**
 * Template Name: Blacklist  Page
 *
 * This is the template that displays for archive  page
 *
 * @package sparkling
 */

get_header(); ?>

<div class="fullwidth blacklist-page">

    <div class="mob-desk-banner fullwidth">
        <div class="fullwidth banner-item banner-desktop"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/blacklist-banner-web.jpg"></div>
        <div class="fullwidth banner-item banner-mobile"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/blacklist-banner-mob.jpg"></div>

    </div>


    <div class="fullwidth page-content">
        <div class="container">
            <div class="row">

                <div class="col-md-8 content-box">

                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>


                <div class="col-md-4">
                    <?php include('sidebar-newsletter-widget.php') ?>
                </div>

            </div>
        </div>
   </div>




    <div class="fullwidth blacklist-data">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $args = array( 'post_type' => 'black_listed', 'posts_per_page'   =>  1);
                    $myposts = get_posts( $args );
                    foreach ( $myposts as $post ) : setup_postdata( $post );
                    ?>

                    <div class="fullwidth blacklistinfo">

                        <div class="blacklist-table-head">
                            <div class="table-left">
                                Name
                            </div>
                            <div class="table-left">
                                Country and date of visa denial
                            </div>
                        </div>



                        <?php
                        if( have_rows('black_list_details') ):
                            while ( have_rows('black_list_details') ) : the_row();
                        ?>


                         <div class="blacklist-item fullwidth">

                            <div class="blacklist-head">
                                <div class="button-box"><a class="blacklistbtn "></a></div>
                                <div class="title-line  same-height-row"><?php the_sub_field('title') ?></div>
                                <div class="sub-line same-height-row">
                                    <div class="location">Country : <br/><?php the_sub_field('country') ?></div>
                                    <div class="date">Date : <br/><?php the_sub_field('date') ?></div>
                                </div>
                            </div>


                            <div class="blacklist-txt">
                                <h3 class="subheading">Explanation</h3>
                                    <?php the_sub_field('description') ?>
                            </div>


                        </div>

                                <?php
                            endwhile;
                        else :
                        endif;

                        ?>


                        <?php endforeach;
                        wp_reset_postdata();
                        ?>


                    </div>

                </div>
            </div>
        </div>

    </div>



</div>

<?php
get_footer();

?>

