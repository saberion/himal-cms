<?php
/**
 * Template Name: Seylan IPG Confirm Page
 *
 * This is the template that Seylan IPG Confirm page
 *
 */

get_header();

$users_to_pay_id = $_REQUEST['users_to_pay_id'];

$userToPay = $wpdb->get_results("SELECT * FROM hmg_users_to_pay WHERE id=$users_to_pay_id");

$membershipId = $userToPay[0]->membership_id;

$pgdomain = "fedpg-onus.pc.enstage-sas.com";
$pgInstanceId = 37166521;
$merchantId = 94024444;
$hashKey = "5F5C3CA127EE21AE";

$perform = "initiatePaymentCapture#sale";
$currencyCode = 840;
$amount = $userToPay[0]->billing_amount;
$amountToHash = $amount*100;
$merchantReferenceNo = "TR-U".$users_to_pay_id."-MID".$membershipId."-".time();
$orderDesc = "Membership Payment";

$messageHash = $pgInstanceId."|".$merchantId."|".$perform."|".$currencyCode."|".$amountToHash."|".$merchantReferenceNo."|".$hashKey."|";

$message_hash = "CURRENCY:7:".base64_encode(sha1($messageHash, true));

?>
    <div class="archive-wrap fullwidth ipg-pages">

        <div class="fullwidth payment-title-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 title-txt">
                        <p>Your <i>Himal</i> membership is nearly ready...</p>
                    </div>
                    <div class="col-md-12 title-img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/payment-title-img.png"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="fullwidth payment-option-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>You have chosen the following membership package</h2>
                    </div>
                </div>
            </div>
        </div>



        <div class="fullwidth  article-info-box ipg-info-box " >
            <div class="container">
                <div class="row">
                    <div class="col-md-12 article-content-wrap ipg-confirm-box" id="main-article-content">



                        <div class="fullwidth payment-option-top">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 payment-option-list">
                                        <a id="memeberblock-id" data-id="membership-<?php echo $membershipId ?>"></a>


                                        <?php
                                        $args = array( 'post_type' => 'membership_page', 'posts_per_page'   =>  1);
                                        $myposts = get_posts( $args );
                                        foreach ( $myposts as $post ) : setup_postdata( $post );
                                            ?>

                                            <?php
                                            if( have_rows('membership_plans') ):
                                                while  ( have_rows('membership_plans') ) : the_row()  ;
                                                    ?>

                                                    <div class="option-block-wrap fullwidth" id="membership-<?php the_sub_field('link'); ?>">
                                                        <div class="fullwidth">
                                                            <a class="reqst-head">
                                                                <strong>  <?php the_sub_field('name'); ?></strong><br/>
                                                                <span>$  <?php the_sub_field('price'); ?></span>
                                                            </a>
                                                        </div>
                                                        <div class="fullwidth request-more-info" >
                                                            <?php the_sub_field('more_details'); ?>
                                                        </div>
                                                    </div>

                                                    <?php
                                                endwhile;
                                            else :
                                            endif;
                                            ?>

                                        <?php endforeach;
                                        wp_reset_postdata();
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="fullwidth  default-page-data  upme-wrap">
                            <div class="row">
                            <form name="merchantForm" method="post" action="https://<?php echo $pgdomain;?>/AccosaPG/verify.jsp">
                                <div class="col-md-6">
                                    <div class="fullwidth ipg-reqest">
                                        <input type="hidden" name="pg_instance_id" value="<?php echo $pgInstanceId;?>" />
                                        <input type="hidden" name="merchant_id" value="<?php echo $merchantId;?>" />
                                        <input type="hidden" name="perform" value="<?php echo $perform;?>" />
                                        <input type="hidden" name="currency_code" value="<?php echo $currencyCode;?>" />
                                        <input type="hidden" name="amount" value="<?php echo $amountToHash;?>" />
                                        <input type="hidden" name="merchant_reference_no" value="<?php echo $merchantReferenceNo;?>" />
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="fullwidth ipg-reqest">
                                        <input type="hidden" name="order_desc" value="<?php echo $orderDesc;?>" />
                                        <input type="hidden" name="message_hash" value="<?php echo $message_hash;?>" />
                                        <input type="hidden" name="ext1" value="<?php echo $users_to_pay_id;?>" />
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="fullwidth ipg-reqest">
                                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=20744" class="btn-red">Go back to make changes</a>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="fullwidth ipg-reqest">
                                                <input type="submit" class="upme-button" name="btnConfirm" value="Make the payment" />
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();