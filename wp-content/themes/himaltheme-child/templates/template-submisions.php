<?php
/**
 * Template Name: Submission  Page
 *
 * This is the template that displays for submission  page
 *
 * @package sparkling
 */

get_header(); ?>



<div class="fullwidth submission-page inner_page ">

    <div class="container">



        <?php
        $args = array( 'post_type' => 'submissions', 'posts_per_page'   =>  -1, );
        $myposts = get_posts( $args );
        foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

        <div class="row">
            <div class="col-md-12">

                <div class="fullwidth section-heading">
                    <h2><span><?php  the_title(); ?> </span></h2>
                </div>


            <?php
                if( have_rows('submission_details') ): while ( have_rows('submission_details') ) : the_row();
            ?>


                <?php if( get_sub_field('sub_heading') ): ?>

                    <div class="submision_subheading fullwidth"><h3><?php the_sub_field('sub_heading') ; ?></h3></div>
                    <?php endif; ?>
                    <div class="submisioninfobox_wrap fullwidth">

                    <?php
                    if( have_rows('more_details') ): while ( have_rows('more_details') ) : the_row();
                        ?>

                <div class="fullwidth submisioninfobox">
                    <h2 class="subheading"><?php the_sub_field('title') ; ?></h2>
                    <div class="submisioninfo_box"><?php the_sub_field('description') ; ?></div>
                </div>

                        <?php
                    endwhile; endif;
                    ?>
                    </div>


                    <?php
                endwhile; endif;
                ?>




            </div>
        </div>


        <?php endforeach;
        wp_reset_postdata();
        ?>


    </div>

</div>

<?php include('membership.php'); ?>

<?php
get_footer();

?>
