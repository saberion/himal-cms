<?php
/**
 * Template Name: Seylan IPG Response Page
 *
 * This is the template that Seylan IPG Response page
 *
 */

get_header();

global $wpdb;

////////////////

$pgInstanceId = 37166521;
$merchantId = 94024444;
$hashKey = "5F5C3CA127EE21AE";
$currencyCode = 840;

$transactionTypeCode=$_POST["transaction_type_code"];
$installments=$_POST["installments"];
$transactionId=$_POST["transaction_id"];

$amount=$_POST["amount"];
$exponent=$_POST["exponent"];
$currencyCode=$_POST["currency_code"];
$merchantReferenceNo=$_POST["merchant_reference_no"];

$status=$_POST["status"];
$eci=$_POST["3ds_eci"];
$pgErrorCode=$_POST["pg_error_code"];

$pgErrorDetail=$_POST["pg_error_detail"];
$pgErrorMsg=$_POST["pg_error_msg"];

$messageHash=$_POST["message_hash"];


$messageHashBuf=$pgInstanceId."|".$merchantId."|".$transactionTypeCode."|".$installments."|".$transactionId."|".$amount."|".$exponent."|".$currencyCode."|".$merchantReferenceNo."|".$status."|".$eci."|".$pgErrorCode."|".$hashKey."|";

$messageHashClient = "13:".base64_encode(sha1($messageHashBuf, true));

$hashMatch=false;

if ($messageHash==$messageHashClient){
  $hashMatch=true;
} else {
  $hashMatch=false;
}

$users_to_pay_id = $_POST['ext1'];


if("50020"==$status) {
	if ($users_to_pay_id) {

        $userToPay = $wpdb->get_results("SELECT * FROM hmg_users_to_pay WHERE id=$users_to_pay_id");

        $email = $userToPay[0]->email;
        $password = $userToPay[0]->password;

        $userName = $email;
        $user_id = wp_create_user($userName, $password, $email);

        $user = new WP_User($user_id);
        $user->set_role('subscriber');

        $firstName = $userToPay[0]->first_name;
        $lastName = $userToPay[0]->last_name;
        $address = $userToPay[0]->address;
        $city = $userToPay[0]->city;
        $county = $userToPay[0]->county;
        $phone = $userToPay[0]->phone;
        $country = $userToPay[0]->country;
        
        update_user_meta( $user_id, "first_name",  $firstName);
        update_user_meta( $user_id, "last_name",  $lastName);
        update_user_meta( $user_id, "address",  $address);
        update_user_meta( $user_id, "city",  $city);
        update_user_meta( $user_id, "county",  $county) ;
        update_user_meta( $user_id, "phone",  $phone);
        update_user_meta( $user_id, "country",  $country) ;

        $wpdb->insert('hmg_pmpro_memberships_users',array(
            'user_id' => $user_id,
            'membership_id' => $userToPay[0]->membership_id,
            'code_id' => 0,
            'initial_payment' => $userToPay[0]->initial_payment,
            'billing_amount' => $userToPay[0]->billing_amount,
            'cycle_number' => 1,
            'cycle_period' => 'Year',
            'billing_limit' => 0,
            'trial_amount' => 0,
            'trial_limit' => 0,
            'status' => 'active',
            'startdate' => date("Y-m-d") . "T0:0:0",
            'enddate' => date('Y-m-d', strtotime('+1 years'))."T0:0:0"), array('%d', '%d', '%d','%d', '%d', '%d','%s', '%d', '%d','%d', '%s', '%s', '%s'));

        $memberships_users_id = $wpdb->insert_id;

        $wpdb->insert('hmg_pmpro_membership_orders',array(
                    'code' => time(),
                    'session_id' => 'session id',
                    'user_id' => $user_id,
                    'membership_id' => $userToPay[0]->membership_id,
                    'paypal_token' => '',
                    'billing_name' => $firstName." ".$lastName,
                    'billing_street' => 'Street',
                    'billing_city' => $city,
                    'billing_state' => $county,
                    'billing_zip' => '',
                    'billing_country' => $country,
                    'billing_phone' => $phone,
                    'subtotal' => $userToPay[0]->billing_amount,
                    'tax' => 0,
                    'couponamount' => 0,
                    'checkout_id' => 0,
                    'certificate_id' => 0,
                    'certificateamount' => 0,
                    'total' => $userToPay[0]->billing_amount,
                    'payment_type' => '',
                    'cardtype' => '',
                    'accountnumber' => '',
                    'expirationmonth' => '',
                    'expirationyear' => '',
                    'status' => 'success',
                    'gateway' => 'Seylan IPG',
                    'gateway_environment' => 'Production',
                    'payment_transaction_id' => '',
                    'subscription_transaction_id' => '',
                    'timestamp' => time(),
                    'affiliate_id' => '',
                    'affiliate_subid' => '',
                    'notes' => ''), array('%s', '%s', '%d','%d', '%s', '%s','%s', '%s', '%s','%d', '%s', '%s','%d', '%d', '%d','%d', '%d', '%d','%d', '%s', '%s','%s', '%s', '%s','%s', '%s', '%s','%s', '%s', '%s','%d', '%d', '%s'));

        $membership_orders_id = $wpdb->insert_id;
	}
}

// echo "<pre>";
// print_r($_POST);
// exit;

/*

$name = $_POST['message_name'];
$email = "membership@himalmag.com";
$message = $_POST['message_text'];

$to = get_option('admin_email');
$subject = "Some text in subject...";
$headers = 'From: '. $email . "\r\n" .
'Reply-To: ' . $email . "\r\n";

$sent = wp_mail($to, $subject, strip_tags($message), $headers);
if($sent){

}else{

}

*/

?>



<?php
/*

$from = '<membership@himalmag.com>';
$to = '<manoj@saberion.com>';
$subject = 'Hi!';
$body = "Hi Admin, \n\n new user registered?";

$headers = array(
    'From' => $from,
    'To' => $to,
    'Subject' => $subject
);

$smtp = Mail::factory('smtp', array(
    'host' => 'ssl://smtp.gmail.com',
    'port' => '465',
    'auth' => true,
    'username' => 'membership@himalmag.com',
    'password' => 'ZC=7eK%W'
));

$mail = $smtp->send($to, $headers, $body);

if (PEAR::isError($mail)) {
    echo('<p>' . $mail->getMessage() . '</p>');
} else {
    echo('<p>Message successfully sent!</p>');
}
*/
?>


<div class="archive-wrap fullwidth ipg-pages">

    <div class="fullwidth payment-title-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12 title-txt">
                    <p>Hello, welcome to <i>Himal</i> Southasian!</p>
                </div>
                <div class="col-md-12 title-img">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/payment-title-img.png"/>
                </div>
            </div>
        </div>
    </div>

    <div class="fullwidth payment-option-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <?php
                    if("50020"==$status) {
                        ?>
                        <h2>You have successfully completed the payment process.
                            Please use the email address that you’ve used to receive confirmation to login to the Himal Membership page. </h2>
                        <?php
                    } else if("50097"==$status) {
                        ?>
                        <h2>You have successfully completed the payment process.
                            Please use the email address that you’ve used to receive confirmation to login to the Himal Membership page. </h2>
                        <?php
                    } else {
                        ?><h2 color="#FF0000"><b>Transaction Failed</b></h2><?php
                    }
                    ?>


                </div>
            </div>
        </div>
    </div>

    <div class="fullwidth  article-info-box ipg-info-box" >
        <div class="container">
            <div class="row">
                <div class="col-md-12 article-content-wrap" id="main-article-content">
                     

                    <div class="fullwidth  default-page-data ">


                        <table class="response-result" style="display: none">
                            <tr>
                                <td valign="top" align="center">
                                    <?php
                                    if("50020"==$status) {
                                        ?><font color="#339900"><b>Transaction Passed</b></font><?php
                                    } else if("50097"==$status) {
                                        ?><font color="#339900"><b>Test Transaction Passed</b></font><?php
                                    } else {
                                        ?><font color="#FF0000"><b>Transaction Failed</b></font><?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="mainText">
                                    <table   width="100%"  >
                                        <tr>
                                            <td align="right" width="50%" class="lefted">HashMatch</td>
                                            <td align="left" width="50%">: <?php echo $hashMatch;?></td>
                                        </tr>

                                        <tr>
                                            <td align="right" class="lefted">TransactionTypeCode</td>
                                            <td align="left">: <?php echo $transactionTypeCode;?></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="lefted">TransactionId</td>
                                            <td align="left">: <?php echo $transactionId;?></td>
                                        </tr>

                                        <tr>
                                            <td align="right" class="lefted">Amount</td>
                                            <td align="left">: <?php echo $amount/100;?></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="lefted">Exponent</td>
                                            <td align="left">: <?php echo $exponent;?></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="lefted">CurrencyCode</td>
                                            <td align="left">: <?php echo $currencyCode;?></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="lefted">MerchantReferenceNo</td>
                                            <td align="left">: <?php echo $merchantReferenceNo;?></td>
                                        </tr>

                                        <tr>
                                            <td align="right" class="lefted">Status</td>
                                            <td align="left">: <?php echo $status;?></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="lefted">3dsEci</td>
                                            <td align="left">: <?php echo $eci;?></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="lefted">PG ErrorCode</td>
                                            <td align="left">: <?php echo $pgErrorCode;?></td>
                                        </tr>

                                        <tr>
                                            <td align="right" class="lefted">PG ErrorDetail</td>
                                            <td align="left">: <?php echo $pgErrorDetail;?></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="lefted">PG ErrorMsg</td>
                                            <td align="left">: <?php echo $pgErrorMsg;?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();