<div class="populer-news fullwidth" >


    <div class="populer-heading fullwidth"><h2>POPULAR RIGHT NOW</h2></div>

    <div class="populer-info fullwidth">
        <?php
        $popularpost = new WP_Query( array( 'posts_per_page' => 5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
        while ( $popularpost->have_posts() ) : $popularpost->the_post();
            ?>

            <div class="more-item fullwidth">
                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                <h4>By <?php the_author_posts_link(); ?> |  <?php echo get_the_date( 'M d, Y' ); ?>
                </h4>
                <?php if ( ! has_excerpt() ) {
                    ?>

                    <?php
                } else {
                    ?>
                    <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                    <?php
                }
                ?>
            </div>


            <?php
        endwhile;
        ?>

    </div>

</div>