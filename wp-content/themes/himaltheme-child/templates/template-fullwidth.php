<?php
/**
 * Template Name: Full Width  Page
 *
 * This is the template that displays for journalists  page
 *
 * @package sparkling
 */

get_header(); ?>

<div class="archive-wrap fullwidth">






    <div class="fullwidth  article-info-box " >
        <div class="container">
            <div class="row">
                <div class="col-md-12 article-content-wrap" id="main-article-content">


                    <div class="fullwidth heading-box">
                        <div class="container-inner">

                            <div class="fullwidth title-row" >
                                <h1><?php the_title(); ?></h1>
                            </div>

                        </div>
                    </div>





                    <div class="fullwidth  default-page-data ">

                        <?php
                        while ( have_posts() ) : the_post();

                            the_content();

                        endwhile;
                        ?>

                    </div>



                </div>



            </div>

        </div>

    </div>




</div>







<?php
get_footer();

?>
