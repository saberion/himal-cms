<?php
/**
 * Template Name: My Account  Page
 *
 * This is the template that displays for my account  page
 *
 * @package sparkling
 */

get_header(); ?>

<div class="archive-wrap fullwidth ipg-pages">






    <div class="fullwidth  article-info-box " >
        <div class="container">
            <div class="row">
                <div class="col-md-12 article-content-wrap" id="main-article-content">


                    <div class="fullwidth heading-box">
                        <div class="container-inner">

                            <div class="fullwidth title-row" >
                                <h1><?php the_title(); ?></h1>
                            </div>

                        </div>
                    </div>




                    <div class="fullwidth payment-option-top">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 payment-option-list">


                                    <?php
                                    $args = array( 'post_type' => 'membership_page', 'posts_per_page'   =>  1);
                                    $myposts = get_posts( $args );
                                    foreach ( $myposts as $post ) : setup_postdata( $post );
                                        ?>

                                        <?php
                                        if( have_rows('membership_plans') ):
                                            while  ( have_rows('membership_plans') ) : the_row()  ;
                                                ?>

                                                <div class="option-block-wrap fullwidth" id="membership-<?php the_sub_field('link'); ?>">
                                                    <div class="fullwidth">
                                                        <a class="reqst-head">
                                                            <strong>  <?php the_sub_field('name'); ?></strong><br/>
                                                            <span>$  <?php the_sub_field('price'); ?></span>
                                                        </a>
                                                    </div>
                                                    <div class="fullwidth request-more-info" >
                                                        <?php the_sub_field('more_details'); ?>
                                                    </div>
                                                </div>

                                                <?php
                                            endwhile;
                                        else :
                                        endif;
                                        ?>

                                    <?php endforeach;
                                    wp_reset_postdata();
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>





                    <div class="fullwidth  default-page-data ">

                        <?php
                        while ( have_posts() ) : the_post();

                            the_content();

                        endwhile;
                        ?>

                    </div>



                </div>



            </div>

        </div>

    </div>




</div>







<?php
get_footer();

?>
