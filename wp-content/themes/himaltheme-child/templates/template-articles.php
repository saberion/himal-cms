<?php
/**
 * Template Name: All Articles  Page
 *
 * This is the template that displays for careers  page
 *
 * @package sparkling
 */

get_header(); ?>


<div class="category-wrap fullwidth">
<div class="first-articlewrap  fullwidth">
    <div class="container">



        <div class="row search-bar">
            <div class="col-md-12 search-box">
                <div class="search-field">
                    <?php
                    get_search_form();
                    ?>
                </div>

            </div>



        </div>



        <div class="remaining-articlewrap  fullwidth">
            <div class="container">
                <div class="row">

                    <div class="col-md-8" id="loadmore-wrap">

                        <?php
                        $args = array( 'post_type' => 'post', 'posts_per_page'   => 8,   );
                        $myposts = get_posts( $args );
                        foreach ( $myposts as $post ) : setup_postdata( $post );
                        ?>

                        <div class="news-item loadmore-item">

                            <a href="<?php the_permalink(); ?>">
                                <?php if (has_post_thumbnail()) { ?>
                                    <div class="news-img" style='background-image: url("<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>")'>   </div>
                                <?php } else { ?>
                                    <div class="news-img"><img
                                            src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-img.png">
                                    </div>
                                <?php } ?>
                            </a>

                            <div class="news-txt">
                                <div class="taglist"><?php the_category(); ?></div>

                                <div class="fullwidth content-except">
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <?php if ( ! has_excerpt() ) {
                                    ?>
                                    <p><?php echo clean_excerpt(100);  ?> </p>
                                    <?php
                                } else {
                                    ?>
                                    <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                                    <?php
                                }
                                ?>
                                </div>
                                <div class="fullwidth">
                                <h4><?php the_author_posts_link(); ?> |  <?php echo get_the_date( 'M d, Y' ); ?></h4>
                                </div>
                            </div>


                        </div>


                        <?php endforeach;
                        wp_reset_postdata();
                        ?>

                        <div class="row more-articles-wrap">
                            <div class="col-md-12">

                            <?php
                            echo do_shortcode('[ajax_load_more id="home-more-wrap" container_type="div" css_classes="more-articles-wrap" post_type="post" posts_per_page="8" offset="8" pause="true" scroll="false" transition_container_classes="more-article-item" images_loaded="true" button_label="Load More" button_loading_label="Loading..."]')
                            ?>

                            </div>
                        </div>



                    </div>

                    <div class="col-md-4 newsleter-side-widget">

                        <div class="fullwidth"> <?php include('latest-side-widget.php') ?>   </div>
                        <div class="fullwidth"><?php include('sidebar-newsletter-widget.php') ?></div>

                    </div>


                </div>
            </div>
        </div>




    </div>
</div>

</div>


<?php include('membership.php'); ?>


<?php
get_footer();

?>
