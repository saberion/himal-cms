<div class="latest-news fullwidth" >


    <div class="populer-heading fullwidth"><h2><span>Latest Articles</span></h2></div>

    <div class="populer-info fullwidth">
        <?php
        $args = array( 'post_type' => 'post', 'posts_per_page'   => 4 );
        $myposts = get_posts( $args );
        foreach ( $myposts as $post ) : setup_postdata( $post );
            ?>

            <div class="more-item fullwidth">
                <div class="taglist"><?php the_category(); ?></div>
                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                <div class="fullwidth content-except">
                <?php if ( ! has_excerpt() ) {
                    ?>

                    <?php
                } else {
                    ?>
                    <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                    <?php
                }
                ?>
                </div>

                <div class="fullwidth"><h4><?php the_author_posts_link(); ?> </h4></div>
            </div>


        <?php endforeach;
        wp_reset_query();
        ?>

    </div>

</div>