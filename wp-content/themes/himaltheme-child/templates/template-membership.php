<?php
/**
 * The template for displaying the membership.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Membership Page
 *
 * @package storefront
 */

get_header(); ?>


<div class="membership-page-wrap fullwidth">
    <div class="backgrond-bar"> </div>

    <div class="fullwidth membership-intro">


        <div class="container">
            <div class="row">
                <div class="col-md-12 membership-txt">
                    <div class="membership-inner membership-inner-red fullwidth">
                    <h3>Be informed.	Be challenged. Be a member.</h3>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-12">


                    <?php
                    $args = array( 'post_type' => 'membership_page', 'posts_per_page'   =>  1);
                    $myposts = get_posts( $args );
                    foreach ( $myposts as $post ) : setup_postdata( $post );
                        ?>

                        <div class="fullwidth membership-other-info ">

                            <div class="fullwidth packages-list">


                                    <div class="row">
                                        <div class="col-md-12 membership-title"><h2>There's an option for everyone</h2></div>
                                    </div>
                                    <div class="row membership-blocks">


                                        <?php
                                        if( have_rows('membership_plans') ):
                                            $i = 1;
                                            while  ( have_rows('membership_plans') ) : the_row()  ;
                                                ?>
                                                <div class="col-md-4 same-height-row membership-option">
                                                    <div class="membership-item">
                                                        <h3> <?php the_sub_field('name'); ?></h3>
                                                        <div class="more-data">
                                                            <h4>$  <?php the_sub_field('price'); ?></h4>
                                                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=23490&membership_id=<?php the_sub_field('link'); ?>" class="more-link">Join</a>
                                                            <div class="fullwidth"><a href="#membership-<?php echo $i ?>" data-lity class="more-data">What's included</a></div>
                                                        </div>
                                                    </div>

                                                    <div class="membership-more">
                                                        <div class="membership-data" id="membership-<?php echo $i ?>">
                                                            <div class="membership-data-in">
                                                                <h3><?php the_sub_field('name'); ?></h3>
                                                                <h4>$  <?php the_sub_field('price'); ?></h4>
                                                                <div class="fullwidth">
                                                                    <?php the_sub_field('more_details'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <?php
                                                $i++;
                                            endwhile;
                                        else :
                                        endif;
                                        ?>

                                    </div>
                            </div>

                        </div>

                    <?php endforeach;
                    wp_reset_postdata();
                    ?>



                </div>
                </div>



<?php
$args = array( 'post_type' => 'membership_page', 'posts_per_page'   =>  1);
$myposts = get_posts( $args );
foreach ( $myposts as $post ) : setup_postdata( $post );
    ?>



    <div class="row">
        <div class="col-md-12 ">
            <div class="membership-inner fullwidth">

            <div class="fullwidth section-heading">
                <h2><span>FAQS</span></h2>
            </div>


            <div class="fullwidth faq-wrapper">

                <?php
                if( have_rows('faqs') ):
                    while ( have_rows('faqs') ) : the_row();
                        ?>
                        <div class="faq-item">
                            <h2 class="faq-quize">
                                <?php the_sub_field('question'); ?>
                            </h2>

                            <div class="fullwidth faq-answer">
                                <?php the_sub_field('answer'); ?>
                            </div>
                        </div>
                        <?php
                    endwhile;
                else :
                endif;
                ?>

            </div>

            </div>
        </div>
    </div>


    <div class="row readers-details">
        <div class="col-md-12">
            <div class="membership-inner fullwidth">

            <div class="fullwidth section-heading">
                <h2><span>Still not convinced?<br/>Here’s what our readers have to say. </span></h2>
            </div>


            <div class="fullwidth readers-slider">

                <?php
                if( have_rows('what_readers_say') ):
                    while ( have_rows('what_readers_say') ) : the_row();
                        ?>

                        <div class="slider-item">
                            <div class="slider-item-inner same-height">


                                <div class="reader-txt">
                                    <p><?php the_sub_field('description'); ?></p>
                                    <h3><?php the_sub_field('name'); ?></h3>
                                </div>

                            </div>
                        </div>


                        <?php
                    endwhile;
                else :
                endif;
                ?>

            </div>
            </div>
        </div>
    </div>


<?php endforeach;
wp_reset_postdata();
?>



        </div>




        <div class="mob-desk-banner fullwidth mob-desk-banner-white">
            <div class="fullwidth banner-item banner-desktop"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Membership-banner_Web.jpg"></div>
            <div class="fullwidth banner-item banner-mobile"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Membership-banner_mobile.jpg"></div>

        </div>





</div>

<?php
get_footer();