


    <?php
    if( have_rows('magazine_issues') ):
    while ( have_rows('magazine_issues') ) : the_row();
    ?>

        <div class="col-md-3 col-sm-6 col-xs-12 same-height-row  magazinepage-item">

            <?php
            $fieldend = get_sub_field_object('end_month');
            $valueend = get_sub_field('end_month');
            $labelend = $fieldend['choices'][ $valueend ];


            $fieldstr = get_sub_field_object('start_month');
            $valuestr = get_sub_field('start_month');
            $labelstr = $fieldstr['choices'][ $valuestr ];
            ?>


            <div class="magzine_box">
                <?php if( get_sub_field('end_month') ):   ?>

                <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=19787&str<?php the_title(); ?>&stm<?php the_sub_field('start_month') ?>&enm<?php the_sub_field('end_month') ?>">

                    <?php else:   ?>

                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=19787&str<?php the_title(); ?>&stm<?php the_sub_field('start_month') ?>&enm<?php the_sub_field('start_month') ?>">


                    <?php endelse; endif; ?>

                    <div class="magzine-img "  ><img src="<?php the_sub_field('cover_image') ?>"></div>
                    <div class="magzine-txt">
                        <span><?php   echo $labelstr ?>

                                <?php if( get_sub_field('end_month') ):   ?>
                                 - <?php   echo $labelend ?>
                                <?php endif; ?><br/>
                            <?php the_title(); ?></span></div>
                </a>
            </div>
        </div>


    <?php
    endwhile;
    endif;
    ?>




