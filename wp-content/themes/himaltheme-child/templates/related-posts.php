<div class="relatedarticle-box fullwidth">
    <div class="container">

        <div class="row ">
            <div class="col-md-12">
                <div class="fullwidth section-heading">
                    <h2><span>Related Stories</span></h2>
                </div>
            </div>
        </div>

        <div class="row">




            <?php
            $exclude = get_the_ID();
            global $wp_query;
            $exclude = $wp_query->post->ID;


            $terms = get_the_terms( $post->ID, 'category' );
            if ( $terms && ! is_wp_error( $terms ) ) :
                if ( ! empty( $terms ) ) {
                    $parentcat =  $terms[0]->slug;
                }?>
            <?php endif;


            $args = array(
                'post_type' => 'post', 'product_cat' => $parentcat, 'posts_per_page'   =>  4, 'orderby' => 'rand',
            );
            $your_query = new WP_Query( $args );
            while( $your_query->have_posts() ) : $your_query->the_post();
                if( $exclude != get_the_ID() ) {
                    ?>
                    <?php
                    $rauthor_id = $post->post_author;
                    ?>


                    <div class="col-md-3 col-sm-6 col-xs-12  article-item ">
                        <div class="artilce-wrap fullwidth">

                            <a href="<?php the_permalink(); ?>">
                                <?php if ( has_post_thumbnail() ) { ?>
                                    <div class="article-img" style='background-image: url("<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>")'></div>
                                <?php } else {?>
                                    <div class="article-img" style='background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/images/default-img.png")'></div>

                                <?php }  ?>
                            </a>


                            <div class="article-txt same-height-row">
                                <div class="taglist"><?php the_category(); ?></div>
                                <div class="titlebox">
                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                </div>
                                <div class="descriptions"> <?php if ( ! has_excerpt() ) {
                                        ?>

                                        <?php
                                    } else {
                                        ?>
                                        <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                                        <?php
                                    }
                                    ?>  </div>
                                <div class="titlebox">
                                <span>  <a href="<?php echo esc_url( home_url( '/' ) ); ?>?author=<?php echo get_the_author_meta( 'ID'  , $rauthor_id ); ?>" >
                                            <?php echo get_the_author_meta('display_name', $rauthor_id); ?></a> | <?php echo get_the_date( 'M d, Y' ); ?> </span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <?php
                }
            endwhile;
            ?>



        </div>
    </div>
</div>
