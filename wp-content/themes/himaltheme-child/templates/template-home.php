<?php
/**
 * Template Name: Home Page
 *
 * This is the template that displays for home page
 *
 * @package sparkling
 */

get_header(); ?>



<div class="fullwidth home-page ">





    <div class="fullwidth memberbox-home">
        <div class="container">
            <div class="row">
                 <div class="col-md-12 memberbox-block" >
                     <div class="fullwidth"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/become-member.png"></div>
                     <div class="fullwidth home-member-btn"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=20744">Become a Member</a></div>
                 </div>
            </div>
        </div>
    </div>


    <div class="home-slider fullwidth">
        <div class="container">
            <div class="row">

            <div class="col-md-7 height-child home-slider-wrap pull-right">
                <div class="slider-images  ">

                    <?php
                    $args = array( 'post_type' => 'post', 'posts_per_page'   =>  4,
                        'tax_query' => array(
                            array (
                                'taxonomy' => 'home_visibility',
                                'field' => 'slug',
                                'terms' => 'show-on-slider',
                            ) ),
                        );
                    $myposts = get_posts( $args );
                    foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                    <div class="slider-item height-child" style='background-image: url("<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>")'>
                        <a href="<?php the_permalink(); ?>"></a>

                    </div>

                    <?php endforeach;
                    wp_reset_postdata();
                    ?>

                </div>
            </div>


            <div class="col-md-5 height-main">

                <div class="slider-description-inner ">
                    <div class="slider-description">
                    <?php
                    $args = array( 'post_type' => 'post', 'posts_per_page'   =>  4,
                        'tax_query' => array(
                            array (
                                'taxonomy' => 'home_visibility',
                                'field' => 'slug',
                                'terms' => 'show-on-slider',
                            ))
                         );
                    $myposts = get_posts( $args );
                    foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                        <div class="slider-item">
                            <div class="taglist"><?php the_category(); ?></div>
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                            <?php if ( ! has_excerpt() ) {
                                ?>
                                 <?php // echo clean_excerpt(100);  ?>
                            <?php
                            } else {
                                ?>
                                <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                            <?php
                            }
                            ?>
                            <h4><?php the_author_posts_link(); ?>
<!--                                | <span class="date">--><?php // // echo get_the_date( 'M d, Y' ); ?><!--</span>-->
                            </h4>
                        </div>

                    <?php endforeach;
                    wp_reset_postdata();
                    ?>
                    </div>

                </div>
            </div>

            </div>


        </div>
    </div>



        <div class="fullwidth feature-articles">
            <div class="container">
                <div class="row">


                <?php
                $args = array( 'post_type' => 'post', 'posts_per_page'   =>  4,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'home_visibility',
                            'field' => 'slug',
                            'terms' => 'show-below-slideshow',
                        ) ),
                    );
                $myposts = get_posts( $args );
                foreach ( $myposts as $post ) : setup_postdata( $post ); ?>



                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="artilce-wrap fullwidth">
                        <a href="<?php the_permalink(); ?>"><div class="article-img" style='background-image: url("<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>")'></div></a>
                        <div class="article-txt same-height-row">
                            <div class="taglist"><?php the_category(); ?></div>
                            <div class="titlebox">
                                <a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>
                            </div>
                            <div class="descriptions">

                                <?php if ( ! has_excerpt() ) {
                                    ?>

                                    <?php
                                } else {
                                    ?>
                                    <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                                    <?php
                                }
                                ?>

                            </div>
                            <div class="titlebox">
                                <span><?php the_author_posts_link(); ?>
<!--                                    |  --><?php //echo get_the_date( 'M d, Y' ); ?>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>

                <?php endforeach;
                wp_reset_postdata();
                ?>


                </div>

             <!--   <div class="row ">
                    <div class="col-md-12 all-aticle-link">
                        <a href="<?php // echo esc_url( home_url( '/' ) ); ?>?page_id=19738">View All</a>
                    </div>
                </div> -->

            </div>
        </div>




    <div class="mob-desk-banner fullwidth mob-desk-banner-white">
        <div class="fullwidth banner-item banner-desktop"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mediafile-web.jpg"></div>
        <div class="fullwidth banner-item banner-mobile"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mediafile-mob.jpg"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="fullwidth btn-wraper  btn-wrap-mediafile"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>?cat=3713" class="btn-mediafile">Read More</a></div>
                </div>
            </div>
        </div>
    </div>







    <div class="fullwidth featured-block">
        <div class="container">
            <div class="row">
            <div class="col-md-4 height-main">
                <div class="podcast-block">
                    <div class="podcast-title"><h3><span>PODCAST </span>
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>?cat=2813">Listen to All</a>
                        </h3>
                    </div>

                    <div class="posdcast-post-list">

                        <?php
                        $args = array( 'post_type' => 'post', 'posts_per_page'   =>  3, 'category_name' => 'podcast',);
                        $myposts = get_posts( $args );
                        foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                        <div class="podcast-item home-pocast-box">
                            <div class="fullwidth podcast-img"  ><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>"/></div>
                            <div class="podcast-txt fullwidth">
                            <h3>  <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                            <span><?php the_author_posts_link(); ?>   </span>

                            <?php  if( get_field('sound_cloud_id') ): ?>
                           <!-- <iframe id="podcast<?php //the_field('sound_cloud_id'); ?>" width="100%" height="170" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/<?php //the_field('sound_cloud_id'); ?>&color=%23bebebe&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
                                <div class="playSound"></div>
                                <div class="stopSound"></div> -->
                            <?php endif; ?>
                            </div>


                        </div>

                        <?php endforeach;
                        wp_reset_postdata();
                        ?>
                    </div>

                </div>
            </div>

            <div class="col-md-8 height-child">
                <div class="photographblock fullwidth">

                    <?php
                    $args = array( 'post_type' => 'post', 'posts_per_page'   =>  1,
                        'tax_query' => array(
                            array (
                                'taxonomy' => 'home_visibility',
                                'field' => 'slug',
                                'terms' => 'is-featured',
                            ) ), );
                    $myposts = get_posts( $args );
                    foreach ( $myposts as $post ) : setup_postdata( $post ); ?>



                        <a href="<?php the_permalink(); ?>"><div class="photograph-img pull-right" style='background-image: url("<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>")'> </div></a>

                    <div class="photograph-intro">
                        <div class="taglist"><?php the_category(); ?></div>
                        <div class="titlebox">
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <?php if ( ! has_excerpt() ) {
                                ?>

                                <?php
                            } else {
                                ?>
                                <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                                <?php
                            }
                            ?>
                            <span><?php the_author_posts_link(); ?> </span>
                        </div>
                    </div>

                    <?php endforeach;
                    wp_reset_postdata();
                    ?>


                </div>
            </div>

            </div>

        </div>
    </div>





    <div class="archiveblock fullwidth">
        <div class="container">
            <div class="row archive-heading">
                <div class="col-md-12">
                    <h2><span>From Himal Archives</span>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=19681">EXPLORE</a>
                    </h2>
                </div>
            </div>

            <div class="row archive-details">

                <?php
                $args = array( 'post_type' => 'post', 'posts_per_page'   =>  3,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'home_visibility',
                            'field' => 'slug',
                            'terms' => 'is-home-archive',
                        ) ), );
                $myposts = get_posts( $args );
                foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="archive-item same-height">

                        <a href="<?php the_permalink(); ?>"> <div class="archive-img" style='background-image: url("<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>")' ></div></a>



                        <div class="archive-txt">
                            <div class="taglist"><?php the_category(); ?></div>
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <?php if ( ! has_excerpt() ) {
                                ?>

                                <?php
                            } else {
                                ?>
                                <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                                <?php
                            }
                            ?>
                            <strong><?php the_author_posts_link(); ?>  |  <?php echo get_the_date( 'M d, Y' ); ?>  </strong>
                        </div>
                    </div>
                </div>



            <?php endforeach;
            wp_reset_postdata();
            ?>





                <?php
                $args = array( 'post_type' => 'post', 'posts_per_page'   =>  3,  'orderby' => 'rand',
                    'date_query' => array(
                        array(
                            'before'    => 'December 31st, 2011',
                            'inclusive' => true,
                        ),
                    ),

                    'meta_query' => array(
                        array(
                            'key' => '_thumbnail_id',
                            'compare' => 'EXISTS'
                        ),
                    ),
                    );
                $myposts = get_posts( $args );
                foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="archive-item same-height">

                            <a href="<?php the_permalink(); ?>"> <div class="archive-img" style='background-image: url("<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>")' ></div></a>



                            <div class="archive-txt">
                                <div class="taglist"><?php the_category(); ?></div>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <?php if ( ! has_excerpt() ) {
                                    ?>

                                    <?php
                                } else {
                                    ?>
                                    <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                                    <?php
                                }
                                ?>
                                <strong><?php the_author_posts_link(); ?> </strong>
                            </div>
                        </div>
                    </div>



                <?php endforeach;
                wp_reset_postdata();
                ?>

            </div>

        </div>
    </div>





    <div class="feedbackblock fullwidth">
        <div class="container">

            <div class="row feedback-heading">
                <div class="col-md-12">
                    <h2><span>What our readers say</span>
                    </h2>
                </div>
            </div>


            <div class="row feedbackslider">
                <div class="col-md-12">
                    <div class="fullwidth feedbacksliderbox">

                    <?php
                    $args = array( 'post_type' => 'feedbacks', 'posts_per_page'   =>  9, );
                    $myposts = get_posts( $args );
                    foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                    <div class="feedback-item-wrap ">
                        <div class="feedback-inner ">

                            <div class="user-txt same-height-row">
                                <div class="testi_content "><?php the_content(); ?></div>
                                <h2><?php the_title(); ?></h2>
                                <h4><?php the_field('position'); ?></h4>
                            </div>
                        </div>
                    </div>


                    <?php endforeach;
                    wp_reset_postdata();
                    ?>

                    </div>

                </div>
            </div>


        </div>
    </div>





    <div class="fullwidth more-articlebox">
        <div class="container">


            <div class="row more-articles-heading">
                <div class="col-sm-12">
                    <h2>
                        <span>MORE FROM HIMAL</span>

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=19738">VIEW ALL</a>
                    </h2>

                </div>


            </div>

            <div class="row more-articles-wrap">



                <?php
                $args = array( 'post_type' => 'post', 'posts_per_page'   => 8,
                    'tax_query' => array(
                        array(
                            'taxonomy'  => 'home_visibility',
                            'field'     => 'slug',
                            'terms'     => array('show-on-slider','show-below-slideshow'),
                            'operator'  => 'NOT IN')

                    ),

                    );
                $myposts = get_posts( $args );
                foreach ( $myposts as $post ) : setup_postdata( $post );
                ?>

                    <div class="col-md-3 col-sm-6 col-xs-12 more-article-item ">
                        <div class="artilce-wrap fullwidth">
                            <a href="<?php the_permalink(); ?>">
                                <?php if ( has_post_thumbnail() ) { ?>
                                <div class="article-img" style='background-image: url("<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>")'></div>
                                <?php } else {?>
                                    <div class="article-img" style='background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/images/default-img.png")'></div>

                                <?php }  ?>
                            </a>
                            <div class="article-txt same-height-row">
                                <div class="taglist"><?php the_category(); ?></div>
                                <div class="titlebox">
                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                </div>
                                <div class="descriptions"> <?php if ( ! has_excerpt() ) {
                                        ?>

                                        <?php
                                    } else {
                                        ?>
                                        <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                                        <?php
                                    }
                                    ?>  </div>

                                <div class="titlebox">
                                    <span><?php the_author_posts_link(); ?>  </span>
                                </div>
                            </div>
                        </div>
                    </div>


                <?php endforeach;
                wp_reset_postdata();
                ?>
            </div>

            <div class="row more-articles-wrap">

                <?php
                echo do_shortcode('[ajax_load_more id="home-more-wrap" container_type="div" css_classes="more-articles-wrap" post_type="post" posts_per_page="8" offset="8"  taxonomy="home_visibility" taxonomy_terms="show-below-slideshow, show-on-slider" taxonomy_operator="NOT IN" pause="true" scroll="false" transition_container_classes="more-article-item" images_loaded="true" button_label="Load More" button_loading_label="Loading..."]')
                ?>

            </div>
        </div>
    </div>



</div>




    <?php
    get_footer();

    ?>
