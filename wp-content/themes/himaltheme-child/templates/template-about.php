<?php
/**
 * Template Name: About Page
 *
 * This is the template that displays for about page
 *
 * @package sparkling
 */

get_header(); ?>



<div class="fullwidth about-page ">

    <div class="fullwidth about-intro">
        <div class="container">
            <div class="row">

                <div class="col-md-5 about-img">
                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>"/>
                </div>
                <div class="col-md-7 about-txt">
                    <h2>About Us</h2>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>

            </div>
        </div>
    </div>

    <div class="fullwidth about-intro-black">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <span>INDEPENDENT</span>  <span class="midddle"> IRREVERENT </span> <span> INCISIVE</span>
                </div>

            </div>
        </div>
    </div>



    <div class="fullwidth about-contentbox">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="submenu">

                        <ul>
                            <li><a href="#history">HISTORY</a></li>
                            <li><a href="#why-south-asia">WHY 'SOUTHASIA'?</a></li>
                            <li><a href="#right-side-up">RIGHT-SIDE UP MAP</a></li>
                            <li><a href="#our-team">OUR TEAM</a></li>
                            <li><a href="#contact">Contact</a></li>
                        </ul>

                    </div>

                    <div class="about_content">

                        <?php
                        $args = array( 'post_type' => 'about_page', 'posts_per_page'   =>  1, );
                        $myposts = get_posts( $args );
                        foreach ( $myposts as $post ) : setup_postdata( $post ); ?>


                        <!-- History -->

                       <div class="fullwidth target history-block" id="history">

                           <?php
                           $history = get_field('history');
                           if( $history ){  ?>


                           <div class="fullwidth section-heading">
                               <h2><span>History</span></h2>
                           </div>

                           <div class="fullwidth sectiondata">
                               <div class="fullwidth histry-intro">
                                   <p><?php echo $history['introduction']; ?></p>
                               </div>

                               <div class="fullwidth soundcloudaudio">
                                   <?php echo $history['sound_cloud_link']; ?>
                               </div>

                               <div class="fullwidth history-magazin">
                                   <div class="magazin-img">
                                       <div class="fullwidth"><img src="<?php echo $history['magazine_image']; ?>"/></div>
                                       <p><?php echo $history['magazine_image_caption']; ?></p>
                                   </div>
                                   <div class="magazin-txt"><p><?php echo $history['magazine_details']; ?></p></div>
                               </div>

                           </div>

                           <?php
                           }
                           ?>

                       </div>




<!--       why south asia-->

                            <div class="fullwidth target why-southasia-block" id="why-south-asia">


                                    <div class="fullwidth section-heading">
                                        <h2><span>Why 'Southasia'?</span></h2>
                                    </div>

                                    <div class="fullwidth sectiondata">
                                        <div class="fullwidth">
                                            <?php the_field('why_south_asia'); ?>
                                        </div>

                                    </div>


                            </div>



<!-- Rightside up -->

                            <div class="fullwidth target rightsideup-block" id="right-side-up">


                                <div class="fullwidth section-heading">
                                    <h2><span>Right-side-up map</span></h2>
                                </div>

                                <div class="fullwidth sectiondata">
                                    <div class="fullwidth">
                                        <?php the_field('right_sideup'); ?>
                                    </div>

                                </div>


                            </div>



<!--  our Team-->

                            <div class="fullwidth target our-team-block" id="our-team">

                                <div class="fullwidth section-heading">
                                    <h2><span>Our Team</span></h2>
                                </div>

                                <div class="fullwidth sectiondata">
                                    <div class="fullwidth himal_editors">

                                        <div class="row">


                            <?php
                            if( have_rows('our_team') ): while ( have_rows('our_team') ) : the_row();
                                if( have_rows('team_members') ): while ( have_rows('team_members') ) : the_row();
                                            ?>

                                            <div class="col-md-3 col-sm-6 same-height-row editor_item">
                                                 
                                                <div class="member-txt">
                                                    <h3><?php echo get_sub_field('name'); ?></h3>
                                                    <h4><?php echo get_sub_field('position'); ?></h4>
                                                </div>
                                            </div>

                                    <?php
                                endwhile; endif;
                            endwhile; endif;
                            ?>




                                        </div>


                                    </div>

                                </div>


                                <div class="fullwidth other_editors">
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="fullwidth editor-block">
                                                <h2><span>Founding Editor</span></h2>
                                                <?php
                                                if( have_rows('our_team') ): while ( have_rows('our_team') ) : the_row();
                                                if( have_rows('founding_editors') ): while ( have_rows('founding_editors') ) : the_row();
                                                ?>
                                                <h4><?php echo get_sub_field('founding_editor_name'); ?></h4>

                                                    <?php
                                                endwhile; endif;
                                                endwhile; endif;
                                                ?>


                                            </div>


                                            <div class="fullwidth editor-block">
                                                <h2><span>Founding Advisors</span></h2>
                                                <?php
                                                if( have_rows('our_team') ): while ( have_rows('our_team') ) : the_row();
                                                    if( have_rows('founding_advisors') ): while ( have_rows('founding_advisors') ) : the_row();
                                                        ?>
                                                        <div class="halfwidth">
                                                            <h4><?php echo get_sub_field('advisors_name'); ?></h4>
                                                        </div>

                                                        <?php
                                                    endwhile; endif;
                                                endwhile; endif;
                                                ?>


                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="fullwidth editor-block">
                                                <h2><span>Contributing Editors</span></h2>
                                                <?php
                                                if( have_rows('our_team') ): while ( have_rows('our_team') ) : the_row();
                                                    if( have_rows('contributing_editors') ): while ( have_rows('contributing_editors') ) : the_row();
                                                        ?>
                                                        <h4><?php echo get_sub_field('editors_name'); ?></h4>

                                                        <?php
                                                    endwhile; endif;
                                                endwhile; endif;
                                                ?>


                                            </div>

                                        </div>

                                    </div>
                                </div>





                            </div>





                            <!--  contact-->

                            <div class="fullwidth target contact-block" id="contact">

                                <div class="fullwidth section-heading">
                                    <h2><span>Contact</span></h2>
                                </div>



                                <div class="fullwidth sectiondata">

                                    <?php
                                    if( have_rows('contact') ): while ( have_rows('contact') ) : the_row();
                                    if( have_rows('contact_details') ): while ( have_rows('contact_details') ) : the_row();
                                    ?>
                                                <div class="contact-item" style="background-image: url('<?php the_sub_field("icon") ?> ')">
                                                        <?php the_sub_field('details') ?>
                                                </div>

                                        <?php
                                    endwhile; endif;
                                    endwhile; endif;
                                    ?>

                                    <div class="fullwidth isdnblock">

                                        <?php
                                        $contact = get_field('contact');
                                        if( $contact ){  ?>
                                        <h3><?php echo $contact['issn_heading']; ?></h3>
                                        <p><?php echo $contact['issn_details']; ?></p>
                                        <?php } ?>

                                    </div>

                                </div>


                            </div>





                        <?php endforeach;
                        wp_reset_postdata();
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>


</div>

<?php include('membership.php'); ?>

<?php
get_footer();

?>
