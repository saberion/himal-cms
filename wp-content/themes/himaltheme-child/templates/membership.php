


<div class="mob-desk-banner fullwidth mob-desk-banner-yellow">
    <div class="fullwidth banner-item banner-desktop"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Bottom-membership-banner_2_Web.jpg"></div>
    <div class="fullwidth banner-item banner-mobile"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Bottom-membership-banner_2_Mobile.jpg"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="fullwidth btn-wraper t-edit-btn"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=20744" class="join-btn">Join</a></div>
            </div>
        </div>
    </div>
</div>
