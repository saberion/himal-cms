<?php
/**
 * The template for displaying fiction pages.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shapely
 */

get_header(); ?>

<?php
$queried_object = get_queried_object();
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;
$bannerimage = get_field('category_image', $taxonomy . '_' . $term_id);?>

<div class="fictions-wrap fullwidth">


    <div class="fiction_description fullwidth">
        <div class="container">
            <div class="row">
                <div class="col-md-6 fiction-banner">
                    <img src="<?php echo $bannerimage; ?>" />
                </div>
                <div class="col-md-6 fiction-txt">
                    <h2>Stories from<br/>Southasia</h2>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12 fiction-intro ">
                    <?php if ( the_archive_description() ) { ?>
                        <p><?php the_archive_description(); ?></p>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>




    <div class="fullwidth articles-wraps ">
        <div class="container">

            <?php
            $args = array( 'post_type' => 'post', 'posts_per_page'   =>  5,  'category_name' => 'fiction' );
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>


                    <div class="row articles-featured">
                        <div class="col-sm-6 same-height-row article-img">
                            <a href="<?php the_permalink(); ?>">
                                <?php if ( has_post_thumbnail() ) { ?> <img
                                                src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>">
                                <?php } else {?>
                                <img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-img.png">
                                <?php }  ?>
                            </a> 
                        </div>
                        <div class="col-sm-6 same-height-row article-txt">
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <div class="fullwidth content-except">
                            <?php if ( ! has_excerpt() ) {
                                ?>

                                <?php
                            } else {
                                ?>
                                <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                                <?php
                            }
                            ?>
                            </div>
                            <div class="fullwidth">
                                <h4><?php the_author_posts_link(); ?> |  <?php echo get_the_date( 'M d, Y' ); ?></h4>
                            </div>
                        </div>
                    </div>

            <?php endforeach;
            wp_reset_postdata();
            ?>



            </div>
    </div>

    <div class="mob-desk-banner fullwidth mob-desk-banner-white">
        <div class="fullwidth banner-item banner-desktop banner-bc-member"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Membership-banner_Web.jpg"></div>
        <div class="fullwidth banner-item banner-mobile"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Membership-banner_mobile.jpg"></div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="fullwidth btn-wraper"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=20744" class="btn-member">BECOME A MEMBER</a>   </a></div>
                </div>
            </div>
        </div>
    </div>






    <div class="fullwidth magazine-content page-one">
        <div class="container">
            <div class="row">

                <div class="col-md-8" id="loadmore-wrap">

                    <?php

                    if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post();
                        ?>

                        <div class="news-item loadmore-item">
                            <a href="<?php the_permalink(); ?>">
                                <?php if ( has_post_thumbnail() ) { ?>
                                    <div class="news-img"><img
                                                src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>">
                                    </div>
                                <?php } else {?>
                                    <div class="news-img"  ><img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-img.png"></div>
                                <?php }  ?>
                            </a>

                            <div class="news-txt">
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <div class="fullwidth content-except">
                                <?php if ( ! has_excerpt() ) {
                                    ?>

                                    <?php
                                } else {
                                    ?>
                                    <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                                    <?php
                                }
                                ?>
                                </div>
                                <div class="fullwidth">
                                <h4><?php the_author_posts_link(); ?> |  <?php echo get_the_date( 'M d, Y' ); ?></h4>
                                </div>
                            </div>
                        </div>


                      <?php
                    }}
                    ?>


                    <div class="loadmore-wrapper fullwidth"> <?php echo do_shortcode('[ajax-loadmore-button]'); ?></div>

                </div>


                <div class="col-md-4 newsleter-side-widget">
                     <?php include('templates/sidebar-newsletter-widget.php') ?>
                </div>



        </div>
    </div>




</div>



<?php
get_footer();

?>
