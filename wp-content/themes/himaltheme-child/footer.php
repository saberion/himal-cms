<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Shapely
 */

?>

</div><!-- #main -->


<footer id="colophon" class="site-footer footer" role="contentinfo">

    <div class="fullwidth footer-widgets">
        <div class="container">

            <div class="row">

                <div class="col-md-3 col-sm-12 footer-socials">
                    <?php
                    if(is_active_sidebar('footer-widget-1')){
                        dynamic_sidebar('footer-widget-1');
                    }
                    ?>
                </div>

                <div class="col-md-6 col-sm-12 footer-menu-wrap">
                    <?php
                    if(is_active_sidebar('footer-widget-2')){
                        dynamic_sidebar('footer-widget-2');
                    }
                    ?>
                </div>

                <div class="col-md-3 col-sm-12 footer-subscribe">
                    <?php
                    if(is_active_sidebar('footer-widget-3')){
                        dynamic_sidebar('footer-widget-3');
                    }
                    ?>
                </div>


            </div>
            </div>
    </div>

    <div class="fullwidth footer-info">
        <div class="container">
            <div class="row">
                <div class="col-md-6 footer-left">
                    <span>© Copyright <script>document.write(new Date().getFullYear())</script> Himal Southasian</span>
                </div>
                <div class="col-md-6 footer-right">
                    <span>Web design & development - <a href="http://saberion.com" target="_blank">Saberion</a></span>
                </div>
            </div>
        </div>
    </div>



    <a class="btn btn-sm fade-half back-to-top inner-link" href="#top"><i class="fa fa-angle-up"></i></a>
</footer><!-- #colophon -->




</div><!-- #page -->



<div class="subscribwrap">
    <div class="fullwidth subscribe-box" id="subscribepopup">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="subscribe-wrap-white">
                        <div class="subscribe-wrap">

                            <div class="subscribe-inner">

                                <div class="newsleter-logo">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/himal-newsletter-logo.png"   alt="Himal Southasian">

                                </div>

                                <div class="newsleter-info">
                                    <div class="newsleter-info-txt">
                                        <p><b>Never Miss a Story</b>  stay in the know with fortnightly updates from our newsletters</p>
                                    </div>

                                    <div class="newsleter-info-form">

                                        <form id="subscribe2" method="post" action="https://himalmag.us3.list-manage.com/subscribe/post?u=0c87df9f0948bcfa1bc80d2b4&amp;id=949b3d6c7a" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                                            <fieldset>
                                                <input type="email" value=""  placeholder="Enter Your Email"  class="required email textfield" id="mce-EMAIL" name="EMAIL">
                                                <input type="submit" value="Subscribe" name="subscribe" id="c_sendNow" class="button">
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<?php wp_footer(); ?>

<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>

</body>
</html>