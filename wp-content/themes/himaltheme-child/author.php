<?php
/**
 * The template for displaying authours pages.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shapely
 */

get_header(); ?>



<div class="category-wrap fullwidth authour-wrap">


        <?php
        $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
        ?>



    <div class="remaining-articlewrap  fullwidth">
        <div class="container">
            <div class="row">

                <div class="col-md-8" >

                    <div class="authour-info  fullwidth ">
                        <div class="fullwidth section-heading"><h2><span><?php   echo $curauth->display_name;   ?></span></h2>
                        </div>

                        <div class="fullwidth section-description">
                            <p><?php   echo $curauth->description;   ?></p>
                        </div>

                      <!--  <div class="fullwidth authour-more">
                           <p>ARTICLES BY <strong><?php  // echo $curauth->display_name;   ?></strong></p>
                        </div> -->
                    </div>

                <div class="articleswrap  fullwidth" id="loadmore-wrap">

                    <?php
                    if ( have_posts() ) :
                        while ( have_posts() ) : the_post();
                            ?>


                                <div class="news-item loadmore-item">

                                    <a href="<?php the_permalink(); ?>">
                                        <?php if ( has_post_thumbnail() ) { ?>
                                            <div class="news-img"><img
                                                    src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>">
                                            </div>
                                        <?php } else {?>
                                            <div class="news-img"  ><img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-img.png"></div>
                                        <?php }  ?>
                                    </a>

                                    <div class="news-txt  ">

                                        <div class="taglist"><?php the_category(); ?></div>

                                        <div class="fullwidth content-except">
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <?php if ( ! has_excerpt() ) {
                                            ?>

                                            <?php
                                        } else {
                                            ?>
                                            <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                                            <?php
                                        }
                                        ?>
                                        </div>

                                            <div class="fullwidth ">
                                                    <h4><?php echo get_the_date( 'M d, Y' ); ?></h4>
                                            </div>
                                    </div>


                                </div>




                            <?php
                        endwhile;
                    else :
                    endif;
                    ?>


                    <div class="loadmore-wrapper fullwidth"> <?php echo do_shortcode('[ajax-loadmore-button]'); ?></div>



                </div>

                </div>

                <div class="col-md-4 newsleter-side-widget">

                    <div class="fullwidth">  <?php include('templates/latest-side-widget.php') ?>   </div>
                    <div class="fullwidth"><?php include('templates/sidebar-newsletter-widget.php') ?></div>

                </div>


            </div>
        </div>
    </div>



    <div class="mob-desk-banner fullwidth mob-desk-banner-gray">
        <div class="fullwidth banner-item banner-desktop"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Archive-banner_1_Web.jpg"></div>
        <div class="fullwidth banner-item banner-mobile"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Archive-banner_1_Mobile.jpg"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="fullwidth btn-wraper"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=19681" class="btn-archive"> Archives  </a></div>
                </div>
            </div>
        </div>
    </div>






</div>



<?php
get_footer();

?>
