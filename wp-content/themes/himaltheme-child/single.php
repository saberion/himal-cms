<?php
/**
 * The template for displaying archive pages.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shapely
 */

get_header(); ?>

<div class="fullwidth article-fix-info">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7 title-info">

                <?php
                $category = get_the_category();
                ?>
                 <a href="<?php echo esc_url( home_url( '/' ) ); ?>?cat=<?php echo $category[0]->cat_ID?>"><?php   echo $category[0]->cat_name; ?></a>
                <strong> > </strong>
                <?php the_title(); ?>

            </div>

            <div class="col-md-4 col-sm-5 share-info">
                <ul>
                    <li><a href="<?php the_permalink(); ?>"  class="share-fb" title="<?php the_title();  ?>"><img src="<?php echo get_stylesheet_directory_uri( home_url( '' ) ); ?>/images/share-fb.png"></a></li>
                    <li><a href="<?php the_permalink(); ?>"  class="share-tw" title="<?php the_title();  ?>"><img src="<?php echo get_stylesheet_directory_uri( home_url( '' ) ); ?>/images/share-tw.png"></a></li>
                    <li><a href="<?php the_permalink(); ?>"  class="share-email" title="<?php the_title();  ?>"><img src="<?php echo get_stylesheet_directory_uri( home_url( '' ) ); ?>/images/share-email.png"></a></li>
                    <li><a  class="share-print"><img src="<?php echo esc_url( get_stylesheet_directory_uri( '' ) ); ?>/images/share-print.png"></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div id="progress-bar" class="progress-bar-line"></div>
</div>




<div class="archive-wrap fullwidth">


    <?php if (get_field('page_banner_type') == 'fullbanner') {
        ?>
    <div class="fullwidth  article-img">

             <div class="fullwidth article-img-big">
                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>"/>
            </div>

        <div class="img-caption fullwidth">
            <div class="container"><p>
                    <?php
                    if (the_post_thumbnail_caption()) {
                    ?>
                        <?php   the_post_thumbnail_caption(); ?>

                    <?php
                        }
                        ?>
                </p>
            </div>
        </div>



    </div>

    <?php }?>





    <div class="fullwidth  article-info-box " >
        <div class="container">
            <div class="row">
                <div class="col-md-8 article-content-wrap" id="main-article-content">


                    <div class="fullwidth heading-box">
                        <div class="container-inner">

                       <!-- <div class="fullwidth top-row" >
                            <a href="#" class="backbtn">Back</a>
                        </div>-->

                        <div class="taglist"><?php the_category(); ?></div>

                        <div class="fullwidth title-row" >
                            <h1><?php the_title(); ?></h1>

                            <?php if ( ! has_excerpt() ) {
                                ?>
                                <?php
                            } else {
                                ?>
                                <h3><?php echo get_excerpt_by_id($post->ID);  ?></h3>

                                <?php
                            }
                            ?>
                        </div>

                        <div class="fullwidth sub-row" >
                            <?php
                            $author_id = $post->post_author; 
                            ?>


                            <span class="author"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?author=<?php echo get_the_author_meta( 'ID'  , $author_id ); ?>" > <?php the_author_meta( 'display_name' , $author_id ); ?> </a>  </span> |
                              <span class="date"><?php echo get_the_date( 'M d, Y' ); ?></span>
                        </div>

                        </div>
                    </div>



                    <?php if (get_field('page_banner_type') == 'smallbanner') {
                        ?>
                        <div class="fullwidth  article-img">


                                    <div class="fullwidth article-img-small">
                                        <div class="container-inner">

                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>"/>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="img-caption fullwidth">
                                                         <p>
                                                                    <?php
                                                                    if (the_post_thumbnail_caption()) {
                                                                        ?>
                                                                        <?php   the_post_thumbnail_caption(); ?>

                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                        </div>

                    <?php }?>





                    <div class="fullwidth article-txt">



                        <div class="article-line">

                                <?php  if( get_field('sound_cloud_id') ): ?>
                                    <div class="fullwidth">
                                    <iframe width="100%" height="170" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/<?php the_field('sound_cloud_id'); ?>&color=%23bebebe&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
                                    </div>
                                <?php endif; ?>


                            <div class="fullwidth article-line-txt">
                                <div class="fullwidth article-content-block-wrap"  id="single-page-content">

                                    <div class="fullwidth article-content-block">


                                        <?php while ( have_posts() ) : the_post(); ?>
                                            <?php the_content(); ?>
                                            <?php
                                        endwhile;
                                        wp_reset_query();
                                        ?>
                                    </div>

                                    <?php for($i=2;$i<=5;$i++){
                                        if( get_field('wpcf-article-editor-'.$i) ){
                                        ?>

                                      <div class="fullwidth article-content-block">

                                          <?php
                                           $section_content = get_field('wpcf-article-editor-'.$i, false, false);
                                           $section_content = apply_filters('the_content', $section_content);
                                         echo $section_content;
                                          ?>
                                     </div>

                                        <?php
                                    }}
                                    ?>
                                </div>



                            </div>


                            <div class="fullwidth article-meta">
                                <div class="auhtour-info fullwidth">
                                    <span class="author"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?author=<?php echo get_the_author_meta( 'ID'  , $author_id ); ?>" > <?php the_author_meta( 'display_name' , $author_id ); ?> </a>  </span>

                                </div>


                                <?php $author_info = get_the_author_meta( "description"  , $author_id );  ?>

                                <?php if( !empty($author_info)){  ?>

                                <div class="auhtour-disc fullwidth">
                                    <p><?php echo $author_info; ?></p>
                                </div>
                                <?php } ?>


                                <div class="article-tags fullwidth">
                                    <?php the_tags('Topics : ', ', ', ''); ?>
                                </div>

                                <div class="fullwidth comment-btn">
                                    <a class="coomentbtn">Comments</a>
                                </div>
                            </div>


                        </div>

                    </div>


                    <div class="fullwidth article-txt">



                        <div class="fullwidth comment-line">

                            <?php
                            if ( comments_open() || get_comments_number() ) :
                                comments_template();
                            endif;

                            ?>
                        </div>
                    </div>



                    <div class="mob-desk-banner fullwidth show-mobile">
                        <div class="fullwidth banner-item banner-desktop banner-bc-member"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Membership-banner_Web.jpg"></div>
                        <div class="fullwidth banner-item banner-mobile"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Membership-banner_mobile.jpg"></div>

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="fullwidth btn-wraper"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=20744" class="btn-member">BECOME A MEMBER</a>   </a></div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>


                <div class="col-md-4" id="main-article-sidebar">
                    <?php include('templates/latest-side-widget.php') ?>

                    <div class="fullwidth newslterwrap-single">
                        <?php include('templates/sidebar-newsletter-widget.php') ?>
                    </div>

                </div>
            </div>

        </div>

    </div>

<div class="fullwidth single-btm">






    <div class="mob-desk-banner fullwidth hide-mobile mob-desk-banner-white">
        <div class="fullwidth banner-item banner-desktop banner-bc-member"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Membership-banner_Web.jpg"></div>
        <div class="fullwidth banner-item banner-mobile"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Membership-banner_mobile.jpg"></div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="fullwidth btn-wraper"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=20744" class="btn-member">BECOME A MEMBER</a>   </a></div>
                </div>
            </div>
        </div>
    </div>






  <?php include('templates/related-posts.php'); ?>




    <div class="mob-desk-banner fullwidth mob-desk-banner-gray">
        <div class="fullwidth banner-item banner-desktop"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Archive-banner_1_Web.jpg"></div>
        <div class="fullwidth banner-item banner-mobile"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/statick-banner/Archive-banner_1_Mobile.jpg"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="fullwidth btn-wraper archiv-wrapper "> <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=19681" class="btn-archive"> Archives  </a></div>
                </div>
            </div>
        </div>
    </div>




</div>




<?php include('templates/membership.php'); ?>
</div>

<?php
get_footer();

?>
