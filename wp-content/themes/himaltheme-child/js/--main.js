jQuery(function($) {
    //slice footer menu list

        $("#menu-footer-menu").each(function() {
            var list = $(this);
            var size = 7;
            var current_size = 0;
            list.children().each(function() {
                if (++current_size > size) {
                    var new_list = $("<ul class='menu'></ul>").insertAfter(list);
                    list = new_list;
                    current_size = 1;
                }
                list.append(this);
            });
        });



    //wrapping newsletter txt
    $('.widget_text .widget-title').each(function(){
        var $this = $(this), text=$this.text().trim(), words = text.split(/\s+/);
        var lastWord = words.pop();
        words.push('<span>' + lastWord + '</span>');
        $this.html(words.join(' '));
    });

    //article progress
    $("#progress-bar").onscroll();
    if ( $('.article-fix-info').length) {
        progressbar = $('.article-fix-info').offset().top -60;
        $(window).scroll(function () {
            if ($(window).scrollTop() > progressbar) {
                $('.article-fix-info').addClass('fixed');
            }
            else {
                $('.article-fix-info').removeClass('fixed');
            }
        });
    }

    //main slider
    $('.slider-images').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 5000,
        asNavFor: '.slider-description'
    });

    $('.slider-description').slick({
        asNavFor: '.slider-images',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
    });

    //testimonials slider
    $('.feedbacksliderbox').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        dots: true,

        responsive: [
            {
                breakpoint: 768,
                settings: {slidesToShow: 2,slidesToScroll: 2,}
            },
            {
                breakpoint: 568,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    });

    $('.readers-slider').slick({
        slidesToShow: 2,
        slidesToScroll: 2,
        arrows: false,
        dots: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 568,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    });

    //home page archive posts
    $('.archive-details .col-md-3').slice(3).remove();
    //monthly archive next prev links

    //monthly archive on magazine page

    var magmonth = $('.magazin-month').html();
    $('.magblock-img-wrap').find('.month-'+magmonth).addClass('activeblock');
    $('.activeblock').addClass('magzine-img');
    $('.activeblock').prev('.magzine-img-block').addClass('magzine-img-left');
    $('.activeblock').next('.magzine-img-block').addClass('magzine-img-right');

    var prevlink = $('.magzine-img-left').find('.monthlink').attr('href');
    var nextlink = $('.magzine-img-right').find('.monthlink').attr('href');

    $('.activeblock').find('.mg-link-left').attr('href', prevlink);
    $('.activeblock').find('.mg-link-right').attr('href', nextlink);
    $('.activeblock , .magzine-img-left, .magzine-img-right').removeClass('magtopbox');
    $('.activeblock , .magzine-img-left, .magzine-img-right').find('.monthlink').remove();
    $('.magzine-img-left, .magzine-img-right').find('.img-wrap img').remove();
    $('.magzine-img-left, .magzine-img-right').find('.mag-txt a').remove();
    $('.activeblock .mg-link-left:not([href])').remove();
    $('.activeblock .mg-link-right:not([href])').remove();
    $('.magtopbox').remove();
    $('.magazin-month').remove();

    //fixing height issues
    $('.same-height').matchHeight({
        byRow: false,
    });

    $('.same-height-row').matchHeight({
        byRow: true,
    });

    $(window).resize(function() {
        //equal heights
        $('.height-main').each(function(){
            var prntheight = $(this).height();
            $(this).parent().find('.height-child').height(prntheight);
        }) ;
    });

    $(window).resize();
    //mobile menu
    $('#menu-mobile-menu').children('.menu-item-has-children').click(function(){
            $(this).children('ul').slideToggle(200);
    });

    //adding active class category sublist
    function catmenufix(){
        var active_submenu =  $('.current-cat-title').html();
        $('a.sub-menu-item').each(function () {
            if ($(this).html() == active_submenu ) {
                $(this).addClass('active');
            }
        });

        $('.news-txt .post-categories li  a').each(function(){
            if ($(this).html() == active_submenu ) {
                $(this).parent('li').remove();
            }
        });
        $('.content-except p a').contents().unwrap();
    }
    catmenufix();

    //single page pagination
    $('#single-page-content').easyPaginate({
        paginateElement: '.article-content-block',
        elementsPerPage: 1,
        effect: 'slide'
    });

    //loadmore articles
    $('.alm-load-more-btn').click(function(){
        setTimeout(function() {
            $('.same-height-row').matchHeight({
                byRow: true,
            });
        }, 3000);
    });

    $('.bliss_loadmore').click(function(){
        setTimeout(function() {
            catmenufix();
        }, 3000);
    });

    //go to back
    $('.backbtn').click(function(){
        window.history.back();
    });

    //email field placeholder
    $('.es_txt_email').each(function(){
        $(this).attr('placeholder', 'E-mail address');
    })

    //about submenu
    if ( $('.submenu').length) {
        navbar = $('.submenu').offset().top -60;
        $(window).scroll(function () {
            if ($(window).scrollTop() > navbar) {
                $('.submenu').addClass('fixed');
                $('#progress-bar').css('opacity', 0)
            }
            else {
                $('.submenu').removeClass('fixed');
            }

            $('.target').each(function() {
                if($(window).scrollTop() >= $(this).offset().top - 50) {
                    var id = $(this).attr('id');
                    $('.submenu').find('a').removeClass('active');
                    $('.submenu').find('a[href="#'+ id +'"]').addClass('active');
                    $('#progress-bar').css('opacity', 1)
                }
            });
        });
    }

    //submenu scroll
    $('.submenu a').click(function () {
        $('.submenu').find('a').removeClass('active');
        $(this).addClass('active');

        var target = $(this.getAttribute('href'));

        if( target.length ) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top - 40
            }, 1000);
        }
    });

    //submissions pages
    $('.submission-page').find('.submisioninfobox_wrap .submisioninfobox').first().removeClass('submisioninfobox');

    function close_submisionblock() {
        $('.submisioninfobox .subheading').removeClass('active');
        $('.submisioninfobox .submisioninfo_box').slideUp(300).removeClass('open');
    }

    $('.submisioninfobox .subheading').click(function(e) {
        // Grab current anchor value
        var currentAttrValue = $(this).next('.submisioninfo_box');

        if($(e.target).is('.active')) {
            close_submisionblock();
        }else {
            close_submisionblock();
            $(this).addClass('active');
            $(currentAttrValue).slideDown(300).addClass('open');
        }
        e.preventDefault();
    });

    //social share
    $('.share-fb').click(function(){
            var sharelink = $(this).attr('href');
            var sharetitle = $(this).attr('title');
            window.open("https://www.facebook.com/sharer/sharer.php?u="+sharelink+"&t="+sharetitle, '',
                'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
            return false;
        }
    );

    $('.share-tw').click(function()
        {
            var sharelink = $(this).attr('href');
            var sharetitle = $(this).attr('title');
            window.open("https://twitter.com/intent/tweet/?text=Check%20out%20this%20website!&url="+sharelink+"&t="+sharetitle, '',
                'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
            return false; }
    );

    $(".share-email").click(function () {
        var emailtitle = $(this).attr('title');
        window.location.href = "mailto:?subject=" + emailtitle;
    });

    function printData()
    {
        var divToPrint=document.getElementById("main-article-content");
        newWin= window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }

    $('.share-print').on('click',function(){
        printData();
    });

    //membership page
    function close_submisionbfaq() {
        $('.faq-item .faq-quize').removeClass('active');
        $('.faq-item .faq-answer').slideUp(300).removeClass('open');
    }

    $('.faq-item .faq-quize').click(function(e) {
        // Grab current anchor value
        var currentfaqValue = $(this).next('.faq-answer');

        if($(e.target).is('.active')) {
            close_submisionbfaq();
        }else {
            close_submisionbfaq();
            $(this).addClass('active');
            $(currentfaqValue).slideDown(300).addClass('open');
        }
        e.preventDefault();
    });

    //paralex background
    var $el = $(' .membership-intro');
    $(window).on('scroll', function () {
        var scroll = $(document).scrollTop();
        $el.css({
            'background-position':'50% '+(-.4*scroll)+'px'
        });
    });

    //newsletter fixed
    if ( $('.newslterwrap-single').length) {
        newslterfix = $('.newslterwrap-single').offset().top -150;
        $(window).scroll(function () {
            if ($(window).scrollTop() > newslterfix  ) {
                $('.newslterwrap-single').addClass('fixed');
            }
            else {
                $('.newslterwrap-single').removeClass('fixed');
            }
        });
    }

    $('.coomentbtn').click(function(){
        $('.comment-line').slideToggle(300);
    })

    //blacklist
    function close_blacklist() {
        $('.blacklistbtn').removeClass('blacklistopen');
        $('.blacklist-txt').slideUp(300).removeClass('open');
    }

    $('.blacklist-head .blacklistbtn').click(function(e) {
        var currentblacklist = $(this).parent('.button-box').parent('.blacklist-head').next('.blacklist-txt');

        if($(e.target).is('.blacklistopen')) {
            close_blacklist();
        }else {
            close_blacklist();
            $(this).addClass('blacklistopen');
            $(currentblacklist).slideDown(300).addClass('open');
        }
        e.preventDefault();
    });

    // responsive datatable
    function datatableactive(){
        var table = $('.jounaldatatable').DataTable( {
            columnDefs: [
                { responsivePriority: 2, targets: 0 },
                { responsivePriority: 1, targets: 6 }
            ],
            responsive: true,

            drawCallback: function(settings) {
                var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
                pagination.toggle(this.api().page.info().pages > 1);
            }
        } );
    }
    datatableactive();




    $('.dataTables_filter input').each(function(){
        $(this).attr('placeholder', 'Search')
    })

    $('.journal-info').hide();

    $('.journal-heading').click(function () {
        var currenttbale = $(this).parent('.jounal-item').children('.journal-info');

        if ($(this).is('.active')) {
            $('.jounal-item .journal-heading').removeClass('active');
            $('.jounal-item .journal-info').slideUp(300).removeClass('open');
        } else {
            $('.jounal-item .journal-heading').removeClass('active');
            $('.jounal-item .journal-info').slideUp(300).removeClass('open');

            $(this).addClass('active');
            $(currenttbale).slideDown(300).addClass('open');
        }
    });

    // archive hide old articles
    $(".magazin-list .magzine_box_wrap:nth-child(12)").nextAll().remove();
    $('.archive_year').on('change', function () {
        var url = $(this).val();
        if (url) {
            window.location = url;
        }
        return false;
    });

    //remove emty p tags
    $('.article-line p:empty').remove();

    // article adding icon
    $('.article-line-txt > p:last-child').append("<span class='icon-last'></span>");
    $('.article-line-txt blockquote .icon-last').remove();

    //scroll to top
    var offset = 400;
    var duration = 800;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
            jQuery('.back-to-top').fadeIn(300);
        } else {
            jQuery('.back-to-top').fadeOut(300);
        }
    });

    //advanced search
    $('.show-advance').click(function(){
        $(this).parent().hide();
        $('.ad_search_bar ul li').slideDown(200);
    });

    //fiction page even odd classs
    if (window.location.href.indexOf("paged=") > -1) {
        $('.category-fiction .magazine-content.page-one').removeClass('page-one');
    }

    $('.magazine-content.page-one .news-item').slice(0,5).remove();
    $('.articles-wraps .articles-featured:odd').addClass('odd');
    $('.articles-wraps .articles-featured:even').addClass('even');

    //archive page  group categories
    var $ul = $('.magazine-content-wrap > .news-item'),
        catnames = {};

    $ul.each(function(){
        catnames[$(this).data('id')] = '';
    });

    for (catname in catnames) {
        $ul.filter('[data-id='+ catname +']').wrapAll('<div class="category_list"></div>');
    }

    $('.magazine-content-wrap .category_list').each(function(){
        var categoryTitle = $(this).children('.news-item').data('title');
        $(this).prepend('<div class="catetitlebox"><h2>'+categoryTitle+'</h2></div>');

        var cattile = {};
        $(".catetitlebox").each(function(){
            var value = $(this).text();
            if (cattile[value] == null){
                cattile[value] = true;
            } else {
                $(this).remove();
            }
        });
    });



    // magazine load more
    $('.magazin-list').simpleLoadMore({
        item: '.magazinepage-item',
        count: 8,
        itemsToLoad: 8,
        btnHTML: '<div class="fullwidth magazin-btn-wrap"><a href="#" class="load-more-btn">View More</a></div>'
    });

    var magbtn = {};
    $(".magazin-btn-wrap").each(function(){
        var value = $(this).text();
        if (magbtn[value] == null){
            magbtn[value] = true;
        } else {
            $(this).remove();
        }
    });



 /*   $('.home-pocast-box').each(function(){
        var scid = $(this).find('iframe').attr('id');
        var widget1 = SC.Widget(scid);

        $(this).find(".playSound").click(function() {
            widget1.play();
            $(".stopSound").hide();
            $(".playSound").show();
            $(this).hide();
            $(this).parent().find(".stopSound").show();
        });
        $(this).find(".stopSound").click(function() {
            widget1.pause();
            $(this).parent().find(".playSound").show();
            $(this).hide();
        });
    }); */

  $('#btnPay').click(function(event){
        event.preventDefault();

        var membership_id = $('#membership_id').val();

        var userName = $('#userName').val(); 
        var password = $('#password').val();
        var email = $('#email').val();
        var firstName = $('#firstName').val();
        var lastName = $('#lastName').val();
        var address1 = $('#address1').val();
        var address2 = $('#address2').val();
        var city = $('#city').val();
        var county = $('#county').val();
        var postalCode = $('#postalCode').val();
        var phone = $('#phone').val();
        var country = $('#country').val();

        data = {
            'action' : 'my_ajax_function',
            'userName' : userName,
            'email' : email,
            'password' : password,
            'membership_id': membership_id,
            'firstName' : firstName,
            'lastName' : lastName,
            'address1' : address1,
            'address2' : address2,
            'city' : city,
            'county' : county,
            'postalCode' : postalCode,
            'phone' : phone,
            'country' : country,
            'nonce' : main_ajax_object.nonce
        }

        $.post(main_ajax_object.ajax_url, data, function( response ) {
            if(response.success){ 
                window.location = "http://himalmagazine.test.saberion.org/?page_id=23541&user_id="+response.user_id+"&memberships_users_id="+response.memberships_users_id+"&membership_orders_id="+response.membership_orders_id;  
            }else{
                alert('Please try again');
            }       
        }, 'json' );
    });
});
