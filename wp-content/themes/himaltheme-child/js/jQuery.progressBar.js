(function ($) {
    $.fn.onscroll = function (options) {
        // This is the easiest way to have default options.
        var settings = $.extend({
            backgroundColor: "#D8232A",
            height: '5px',
        }, options);
        var mySelector = this.selector;
        this.each(function () {
            $(window).scroll(function () {
                var offsettop = parseInt($(this).scrollTop());
                var parentHeight = parseInt($('.article-line-txt').height() - $(window).height());
                var vscrollwidth = offsettop / parentHeight * 100;
                $(mySelector).css({width: vscrollwidth + '%'});
            });
            $(mySelector).css({
                backgroundColor: settings.backgroundColor,
                height: settings.height,
                position: settings.position
            });
        });
        return this;
    };
}(jQuery));
