<?php
/**
 * The template for displaying archive pages.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shapely
 */

get_header(); ?>

<div class="archive-wrap fullwidth">

    <div class="fullwidth magazine-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="fullwidth magblock-img-wrap">
                        <?php
                        $path = $_SERVER['REQUEST_URI'];
                        $magyear =  array_slice(explode('?m=', $path), -1)[0];
                        $styear = substr($magyear, 0, 4);

                        $strm = substr($magyear, 4, 2);
                        $stmObj   = DateTime::createFromFormat('!m', $strm);
                        $stmonthName = $stmObj->format('F'); // March



                        ?>


                        <?php
                        $args = array( 'post_type' => 'magazines', 'posts_per_page'   =>   1, 'name' => $styear);
                        $myposts = get_posts( $args );
                        foreach ( $myposts as $post ) : setup_postdata( $post );

                        ?>



                            <h2 class="magazin-month"><?php echo $strm ?></h2>


                            <?php
                            if( have_rows('magazine_issues') ):
                                while ( have_rows('magazine_issues') ) : the_row();
                                    ?>

                                    <?php
                                    $fieldend = get_sub_field_object('end_month');
                                    $valueend = get_sub_field('end_month');
                                    $labelend = $fieldend['choices'][ $valueend ];


                                    $fieldstr = get_sub_field_object('start_month');
                                    $valuestr = get_sub_field('start_month');
                                    $labelstr = $fieldstr['choices'][ $valuestr ];
                                    ?>

                                    <div class="magzine-img-block magtopbox  month-<?php the_sub_field('start_month') ?>">

                                        <div class="img-wrap same-height">
                                            <img src="<?php the_sub_field('cover_image') ?>">
                                        </div>


                                        <div class="mag-txt">
                                            <a class="mg-link-left" >
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/archive-left.png"   alt="Himal Southasian">
                                            </a>

                                            <h3>
                                                <?php   echo $labelstr  ?>
                                                <?php if( get_sub_field('end_month') ):   ?>
                                                    - <?php   echo $labelend ?>
                                                <?php endif; ?><br/>
                                                <?php the_title(); ?>
                                            </h3>

                                            <a  class="mg-link-right"  >
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/archive-right.png"   alt="Himal Southasian">
                                            </a>
                                        </div>

                                        <?php if( get_sub_field('end_month') ):   ?>
                                        <a class="monthlink" href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=19787&str<?php the_title(); ?>&stm<?php the_sub_field('start_month') ?>&enm<?php the_sub_field('end_month') ?>">

                                            <?php else:   ?>
                                            <a class="monthlink" href="<?php echo esc_url( home_url( '/' ) ); ?>?m=<?php the_title(); ?><?php the_sub_field('start_month') ?>">
                                                <?php endelse; endif; ?>
                                            </a>
                                    </div>


                                    <?php
                                endwhile;
                            endif;
                            ?>


                        <?php endforeach;
                        wp_reset_postdata();
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="fullwidth memberbox memberbox-article">
        <div class="container">
            <div class="row">
                <div class= "col-md-12 subscribe-button"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=20744">BECOME A MEMBER</a></div>

            </div>
        </div>
    </div>




    <div class="fullwidth magazine-content">
        <div class="container">
        <div class="row">
            <div class="col-md-8 magazine-content-wrap">

                <?php

                if ( have_posts() ) :
		            while ( have_posts() ) : the_post();
                ?>


                        <?php $category = get_the_category();   ?>
            <div class="news-item"   data-id="<?php echo $category[0]->slug; ?>" data-title="<?php    echo $category[0]->cat_name; ?>">

                <a href="<?php the_permalink(); ?>">
                    <?php if ( has_post_thumbnail() ) { ?>
                        <div class="news-img"><img
                                    src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>">
                        </div>
                    <?php } else {?>
                        <div class="news-img"  ><img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-img.png"></div>
                    <?php }  ?>
                </a>

               <div class="news-txt ">

                   <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                   <div class="fullwidth content-except">
                   <?php if ( ! has_excerpt() ) {
                       ?>

                       <?php
                   } else {
                       ?>
                       <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                       <?php
                   }
                   ?>
                   </div>

                   <div class="fullwidth ">
                   <h4><?php the_author_posts_link(); ?> |  <?php echo get_the_date( 'M d, Y' ); ?></h4>
                   </div>
               </div>
            </div>

            <?php
                endwhile;
                else :
                endif;
           ?>


            </div>


            <div class="col-md-4">
                <?php include('templates/latest-side-widget.php') ?>
            </div>

        </div>

        </div>




    <div class="backto_archive fullwidth">
        <div class="container">
            <div class="row">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>?magazines=<?php echo $magyear; ?>">
                    Back To <?php echo $magyear; ?> Archives
                </a>

                <h3>Complete<br/>
                    <span><?php echo $magyear; ?> <?php echo $monthName; ?>  Archives</span></h3>

            </div>
        </div>
    </div>


    </div>






<?php include('templates/membership.php'); ?>

<?php
get_footer();

?>
