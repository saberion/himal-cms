<?php
/**
 * Template Name: About Page
 *
 * This is the template that displays for about page
 *
 * @package sparkling
 */

get_header(); ?>

    <div class="magazine-wrap fullwidth">

        <div class="archive_search fullwidth">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">

                        <div class="ad_search_bar">
                            <?php echo do_shortcode('[searchandfilter id="19740"]'); ?>
                        </div>
                        <div class="show-advance-out">
                            <a class="show-advance">ADVANCED SEARCH</a>
                        </div>


                        <div class="search_info">
                            <h2>Over three decades of reporting,<br/>
                                explaining & arguing Southasia</h2>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="fullwidth magazine-data">
            <div class="container">

                <div class="row magazin-head">
                    <div class="col-md-12">
                        <span>BROWSE OUR ARCHIVES</span>
                        <div class="select_wrap">
                            <select class="archive_year">

                                <option selected hidden="hidden"><?php the_title(); ?></option>
                                <?php
                                $args = array( 'post_type' => 'magazines', 'posts_per_page'   =>  -1,  'orderby'=> 'title', 'order' => 'DESC' );
                                $myposts = get_posts( $args );
                                foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                                    <option value="<?php echo esc_url( home_url( '/' ) ); ?>?magazines=<?php the_title(); ?>" ><?php the_title(); ?></option>

                                <?php endforeach;
                                wp_reset_postdata();
                                ?>

                            </select>
                        </div>
                    </div>
                </div>

                <div class="row magazin-list">


                    <?php  include('templates/monthly_templates.php'); ?>


                </div>


            </div>

        </div>

    </div>

<?php
get_footer();

?>
