<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Shapely
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-48366719-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-48366719-1');
    </script>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'shapely' ); ?></a>

    <header id="masthead" class="site-header" role="banner">

        <div class="header-top fullwidth">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="subscribbox">
                            <a href="#subscribepopup" data-lity>FREE NEWSLETTER</a>
                        </div>
                        <div class="topmenu-wrap">
                            <?php wp_nav_menu( array( 'theme_location' => 'topmenu' ) ); ?>
                            <div class="social-iocns">
                                <a href="https://www.facebook.com/himal.southasian/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-fb.png"></a> <span>|</span>
                            <a href="https://twitter.com/himalistan" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-tw.png"></a> <span>|</span>

                                <?php
                                if ( is_user_logged_in() ) {
                                    ?>
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=21078" class="loginlink">My Account</a>     <a>|</a>
                                    <a href="<?php echo wp_logout_url($redirect_to); ?>" class="upme-logout" >LOGOUT</a>

                                <?php  }
                                else { ?>

                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=22310" class="loginlink">Login</a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="fullwidth main-header">

        <div class="nav-container">


            <nav id="site-navigation" class="main-navigation" role="navigation">
                <div class="container nav-bar">
                    <div class="row">
                        <div class="module left site-title-container">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.svg" class="logo" alt="Himal Southasian">
                            </a>
                        </div>
                        <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
                        <a href="<?php  echo esc_url( home_url( '/' ) ); ?>?page_id=20744" class="member-block-btn">MEMBERSHIP</a>
                            <i class="fa fa-bars"></i>
                        </div>


                        <div class="module-group right">

                            <div class="default-view fullwidth">

                                        <div class="module left">
                                            <?php shapely_header_menu(); // main navigation ?>

                                        </div>
                                        <!--end of menu module-->
                                        <div class="module widget-handle search-widget-handle left hidden-xs hidden-sm">
                                            <div class="search">
                                                <i class="fa fa-search"></i>
                                                <span class="title"><?php esc_html_e( "Site Search", 'shapely' ); ?></span>
                                            </div>
                                            <div class="function"><?php
                                                get_search_form(); ?>
                                            </div>
                                        </div>
                            </div>


                            <div class="fixed-view fullwidth">

                                <div class="module left">
                                    <?php wp_nav_menu( array( 'theme_location' => 'fixedmenu' ) ); ?>

                                    <?php
                                    if ( is_user_logged_in() ) {
                                        ?>
                                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=21078" class="loginlink">My Account</a>
                                        <a class="loginlink">|</a>
                                        <a href="<?php echo wp_logout_url($redirect_to); ?> " class="loginlink" >LOGOUT</a>
                                    <?php  }
                                    else { ?>
                                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=22310" class="loginlink">Login</a>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <!--end of menu module-->
                                <div class="module widget-handle search-widget-handle left hidden-xs hidden-sm">
                                    <div class="search">
                                        <i class="fa fa-search"></i>
                                        <span class="title"><?php esc_html_e( "Site Search", 'shapely' ); ?></span>
                                    </div>
                                    <div class="function"><?php
                                        get_search_form(); ?>
                                    </div>
                                </div>
                            </div>


                            <div class="mobile-view fullwidth">

                                <div class="module left">
                                    <?php wp_nav_menu( array( 'theme_location' => 'mobilemenu' ) ); ?>
                                </div>
                                <!--end of menu module-->
                                <div class="module widget-handle search-widget-handle left hidden-xs hidden-sm">
                                    <div class="search-mobile">
                                       <?php get_search_form(); ?>
                                    </div>

                                    <div class="social-iocns">
                                        <a href="https://www.facebook.com/himal.southasian/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-fb-red.png"></a> <span>|</span>
                                        <a href="https://twitter.com/himalistan" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-tw-red.png"></a>
                                    </div>
                                </div>

                            </div>



                        </div>
                        <!--end of module group-->
                    </div>
                </div>
            </nav><!-- #site-navigation -->
        </div>
        </div>
    </header><!-- #masthead -->


    <div id="content" class="main-container">


