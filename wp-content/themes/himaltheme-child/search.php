<?php
/**
 * The template for displaying fiction pages.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shapely
 */

get_header(); ?>



<div class="category-wrap fullwidth">

    <div class="first-articlewrap  fullwidth">
        <div class="container">

            <div class="row search-title">
                <div class="col-md-12">
                    <h1>Search Results</h1>
                </div>
            </div>

            <div class="row search-bar">
                <div class="col-md-12 search-box">
                    <div class="search-field">
                        <?php
                        get_search_form();
                        ?>
                    </div>

                </div>


                <div class="col-md-12 search-info-box">
                    <p><strong> <?php $count = $GLOBALS['wp_query']->found_posts; echo $count ?></strong>  results found.</p>

                </div>
            </div>




        </div>
    </div>



    <div class="remaining-articlewrap  fullwidth">
        <div class="container">
            <div class="row">

                <div class="col-md-8" id="loadmore-wrap">



                    <?php

                    if ( have_posts() ) :
                        while ( have_posts() ) : the_post();
                            ?>



                                <div class="news-item loadmore-item">

                                    <a href="<?php the_permalink(); ?>">
                                        <?php if ( has_post_thumbnail() ) { ?>
                                            <div class="news-img" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), "thumbnail"); ?>');">  </div>
                                        <?php } else {?>
                                            <div class="news-img"  ><img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-img.png"></div>
                                        <?php }  ?>
                                    </a>

                                    <div class="news-txt">
                                        <div class="taglist"><?php the_category(); ?></div>
                                        <div class="fullwidth content-except">
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <?php if ( ! has_excerpt() ) {
                                            ?>
                                            <p><?php echo clean_excerpt(400);  ?> </p>
                                            <?php
                                        } else {
                                            ?>
                                            <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

                                            <?php
                                        }
                                        ?>
                                        </div>
                                        <div class="fullwidth">
                                        <h4><?php the_author_posts_link(); ?> |  <?php echo get_the_date( 'M d, Y' ); ?></h4>
                                        </div>
                                    </div>


                                </div>




                            <?php
                        endwhile;
                    else :
                        ?>
                        <div class="news-item loadmore-item no-results">
                            <p>Sorry, We couldn't find any results matching your search keywords</p>
                        </div>
                        <?php
                    endif;
                    ?>


                    <div class="loadmore-wrapper fullwidth"> <?php echo do_shortcode('[ajax-loadmore-button]'); ?></div>



                </div>

                <div class="col-md-4 newsleter-side-widget">

                    <div class="fullwidth"> <?php include('templates/latest-side-widget.php') ?>   </div>
                    <div class="fullwidth"><?php include('templates/sidebar-newsletter-widget.php') ?></div>

                </div>


            </div>
        </div>
    </div>



    <div class="backto_archive fullwidth">
        <div class="container">
            <div class="row">
                <div class="col-md-9 single-archive-link">
                    <h3>Want to read more Southasia? Browse over three decades of quality journalism in our archives</h3>
                </div>

                <div class="col-md-3 single-archive-link">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=19681"> Archives  </a>
                </div>

            </div>
        </div>
    </div>






</div>



<?php
get_footer();

?>
