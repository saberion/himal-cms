<div class="news-item loadmore-item">

    <a href="<?php the_permalink(); ?>">
        <?php if ( has_post_thumbnail() ) { ?>
            <div class="news-img"><img
                        src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>">
            </div>
        <?php } else {?>
            <div class="news-img"  ><img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-img.png"></div>
        <?php }  ?>
    </a>

    <div class="news-txt">
        <div class="taglist"><?php the_category(); ?></div>
        <div class="fullwidth content-except">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php if ( ! has_excerpt() ) {
            ?>
            <?php
        } else {
            ?>
            <p><?php echo get_excerpt_by_id($post->ID);  ?></p>

            <?php
        }
        ?>
        </div>
        <div class="fullwidth">
            <h4><?php the_author_posts_link(); ?> |  <?php echo get_the_date( 'M d, Y' ); ?></h4>
        </div>
    </div>


</div>